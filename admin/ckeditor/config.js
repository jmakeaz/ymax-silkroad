/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	
	config.language = 'ko';
	config.toolbarCanCollapse = true;
	
	config.toolbarGroups = [
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		/*{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'editing' ] },
		{ name: 'forms', groups: [ 'forms' ] },*/
		{ name: 'insert', groups: [ 'insert' ] },
		//'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'list', groups: [ 'indent', 'blocks' ] },
		{ name: 'align', groups : [ 'align', 'bidi' ] },
		{ name: 'links', groups: [ 'links' ] },
		'/',
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] }
	];

	config.removeButtons = 'Table,Styles,Language,Paste,Italic,PasteFromWord,Save,Form,Superscript,Subscript,Anchor,Flash,Smiley,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Cut,Copy,Templates,image:advanced;link:advanced,NumberedList,BulletedList';
			
	config.format_tags = 'p;h1;h2;h3;pre';
	config.fontSize_defaultLabel = '16px';
	config.fontSize_sizes = '8px/8px;9px/9px;10px/10px;11px/11px;12px/12px;14px/14px;16px/16px;18px/18px;20px/20px;22px/22px;24px/24px;26px/26px;36px/36px;48px/48px;';

	config.font_names = 'Noto Sans Korean; 돋움; 맑은 고딕; 바탕; 궁서;' + config.font_names; // 폰트 설정

	config.enterMode = CKEDITOR.ENTER_BR;
	config.shiftEnterMode = CKEDITOR.ENTER_P;
	config.startupFocus = false;

	config.image_previewText = '<br/>이미지가 배치될 예시 내용입니다.<br />좌측 하단의 "정렬" 옵션을 선택하여, 이미지가 배치될 형태를 변경해보세요.';

	config.forcePasteAsPlainText = true; 
	config.pasteFromWord=false;
	config.pasteFromWordRemoveFontStyles = true;
	config.pasteFromWordRemoveStyles = true;
	config.pasteFromWordNumberedHeadingToList =false;
	config.keystrokes = [
	  //[ CKEDITOR.CTRL + 86 , 'pastetext' ]
	];

	config.allowedContent = true;
    config.extraAllowedContent = 'span(*)[*]{*};div(*)[*]{*};li(*)[*]{*};ul(*)[*]{*}';
    CKEDITOR.dtd.$removeEmpty.i = 0;

	config.height = '400px';

	/* 확장 플러그인 */
	//config.extraPlugins = 'tableresize';
	config.extraPlugins = 'widgetselection,lineutils,notificationaggregator,filetools,uploadwidget,uploadimage';	
	//config.imageUploadUrl  = '/admin/common/editorUpload.do?type=json';

	config.disallowedContent = 'script; *[on*]; iframe';
};

CKEDITOR.on('dialogDefinition', function (ev) {
            var dialogName = ev.data.name;
            var dialog = ev.data.definition.dialog;
            var dialogDefinition = ev.data.definition;

            if (dialogName == 'image') {    	
                dialogDefinition.removeContents('advanced'); // 자세히탭 제거
                dialogDefinition.removeContents('Link'); // 링크탭 제거
                //dialogDefinition.removeContents('Upload'); // 업로드탭

            	dialog.on('show', function(){
            		this.selectPage('Upload');
            	});
            }
});
/*
CKEDITOR.on('instanceReady', function(ev){
	ev.editor._.commands.paste = ev.editor._.commands.pastetext;
});
*/