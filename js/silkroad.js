		
String.prototype.cut = function(iCount) {
	var strReturn = this;
	var intLength = 0;

	for (var i=0; i<strReturn.length; i++) {
		intLength += (strReturn.charCodeAt(i) > 128) ? 2 : 1;

		if (intLength > iCount)
			return strReturn.substring(0,i) + "..";
	}
	return strReturn;
};

String.prototype.replaceAll = function(source, target) {
	source = source.replace(new RegExp("(\\W)", "g"), "\\$1");
	target = target.replace(new RegExp("\\$", "g"), "$$$$");

	return this.replace(new RegExp(source, "gm"), target);
};

function onlyEngNum(val) {
	var oElement = (arguments[0] != null) ? arguments[0] : this;
	var charChk;

	for(var i=0; i<oElement.value.length; i++){
		charChk = oElement.value.charCodeAt(i);

		if( ((charChk < 65 || charChk > 90) && (charChk < 97 || charChk > 122)) && (charChk > 57 || charChk < 48) ){
			//alert("공백없이 영문과 숫자로만 입력해주세요.");
			oElement.value = oElement.value.substring(0, i);
			oElement.focus();
			return;
		}
	}
	
	/*
	var regExp= /^[a-z\d]+$/;
	if(!regExp.test(oElement.value)){
		oElement.value = oElement.value.substring(0, oElement.value.length-1);
		oElement.focus();
		return;
	}
	*/
}


function comma(str) {
	str = String(str);
	return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
}

function returnAge(dataTime){
	var str = '';
	
	var current = new Date().getTime().toString();
	current = parseInt(current.substring(0, current.length-3));

	var dTime = current-parseInt(dataTime);
	
	var milliMin = 60;
	var milliHour = milliMin*60;
	var milliDay = milliHour*24;
	d = parseInt(dTime / (milliDay));
	h = parseInt( (dTime - (d*milliDay)) / (milliHour));
	m = parseInt( (dTime - (d*milliDay) - (h*milliHour)) / (milliMin));
	s = parseInt( (dTime - (d*milliDay) - (h*milliHour) - (m*milliMin)) );

	if(d>0){
		str = d+' days '+h+' hrs ago';
	}else if(d == 0 && h == 0 && m == 0 ){
		str = s+' secs ago'
	}else {
		str = (h > 0 ? h+' hrs ' : '') + m +' min ago';	
	}
	 return str;
}

function returnMobileAge(dataTime){
	var str = '';
	
	var current = new Date().getTime().toString();
	current = parseInt(current.substring(0, current.length-3));

	var dTime = current-parseInt(dataTime);
	
	var milliMin = 60;
	var milliHour = milliMin*60;
	var milliDay = milliHour*24;
	d = parseInt(dTime / (milliDay));
	h = parseInt( (dTime - (d*milliDay)) / (milliHour));
	m = parseInt( (dTime - (d*milliDay) - (h*milliHour)) / (milliMin));
	s = parseInt( (dTime - (d*milliDay) - (h*milliHour) - (m*milliMin)) );

	if(d>0){
		str = d+' d '+h+' h ago';
	}else if(d == 0 && h == 0 && m == 0 ){
		str = s+' s ago'
	}else {
		str = (h > 0 ? h+' h ' : '') + m +' m ago';	
	}
	 return str;
}

function returnValue(val){
	var str = '0';
	
	if(val && !isNaN(val) && val != "0" ){
		var data = val;
		var left = data.substring(0, data.length-18);
		var right = parseFloat("0."+data.substring(left.length, data.length));
		str = (left != '' ? comma(left) : '0') + (right > 0 ? right.toString().substr(1,right.length) : '');

	}
	return str;
}


function returnValue2(val){
	var str = '0';
	if(val && !isNaN(val) && val != "0" ){
		var data = val;
		if(data.indexOf('.') > 0 ){
			var left = data.substring(0, data.indexOf('.'));
			var right = parseFloat("0."+data.substring(left.length+1, data.length));
			str = (left != '' ? comma(left) : '0') + (right > 0 ? right.toString().substr(1,right.length) : '');
		}else{
			str = comma(data);
		}
	}
	return str;
}


/*
function getEtherBalance(){
	var url = 'https://api.etherscan.io/api?module=account&action=balance&address=${ethAddress}&tag=latest&apikey=${etherscanKey}';
	$.get(
		url,
		function(data){
			if(data.status == 1){
				ethValue = parseFloat(returnValue(data.result).replaceAll(',' , ''));
				$('#ethValue').html(returnValue(data.result));
			}
		}
	);
}

function getSlkBalance(){
	var url = 'https://api.etherscan.io/api?module=account&action=tokenbalance&contractaddress=${contract_address}&address=${ethAddress}&tag=latest&apikey=${etherscanKey}';
	$.get(
		url,
		function(data){
			if(data.status == 1){
				slkValue = parseFloat(returnValue(data.result).replaceAll(',' , ''));
				$('#slkValue').html(returnValue(data.result));
			}
		}
	);
}
*/



//-----------------------------------------------------------------------------
// 쿠키저장
// @return : null
// ex) SetCookie(쿠키이름, 쿠키값, 만료기간);
//-----------------------------------------------------------------------------
function SetCookie(name, value, expiredays){//쿠키 설정
	var todayDate = new Date(); 

	todayDate.setDate( todayDate.getDate() + expiredays ); 
	document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";";
} 



//-----------------------------------------------------------------------------
// 쿠키추출
// @return : null
// ex) GetCookie(쿠키이름);
//-----------------------------------------------------------------------------
function GetCookie(name){
	var arg = name + "=";
	var alen = arg.length; 
	var clen = document.cookie.length;
	var i = 0;

	while (i < clen) {
		var j = i + alen; 

		if(document.cookie.substring(i, j) == arg){
			var endstr = document.cookie.indexOf (";", j);
			if(endstr == -1) 
				endstr = document.cookie.length; 

			return unescape(document.cookie.substring(j, endstr));
		}

		i = document.cookie.indexOf(" ", i) + 1;
		if (i == 0) break;
	}

	return null;
} 



//-----------------------------------------------------------------------------
// 쿠키삭제
// @return : null
// ex) DeleteCookie(쿠키이름);
//-----------------------------------------------------------------------------
function DeleteCookie(name){
	var exp = new Date(); 
	var cval = GetCookie(name);

	exp.setTime(exp.getTime() - 1); 
	document.cookie = name + "=" + cval + "; expires=" + exp.toGMTString(); 
}