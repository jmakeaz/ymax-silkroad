$(document).ready(function() {
	var langCode = location.pathname.split('/')[1];	
	var langArray = "en|ko|cn|jp";
	if(langCode == null || langArray.indexOf(langCode) < 0){
		langCode = 'en';
	}

	/*
	var langCode = 'en';
	if(	$('.header-language a').eq(0).hasClass('active') ) {
		langCode = 'ko';
	}

	*/
	$("#join1submit").on("click",function (event) {    
		if($('input:checkbox[id="check01"]').is(":checked") == false){
			//alert('이용약관에 동의하여 주십시요.');
			alert(eval('msgP.'+langCode+'.termsAgree'));
			return false;
		}
		if($('input:checkbox[id="check02"]').is(":checked") == false){
			//alert('개인정보 수집 및 이용에 동의하여 주십시요.');
			alert(eval('msgP.'+langCode+'.privacyAgree'));
			return false;
		}
		$(location).attr('href', '/'+langCode+'/User/regiForm');
    });
	
	$("#join1back").on("click",function (event) {    
		event.preventDefault();
		history.back(1);
    });
});