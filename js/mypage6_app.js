

$(document).ready(function() {
	var langCode = location.pathname.split('/')[1];	
	var langArray = "en|ko|cn|jp";
	if(langCode == null || langArray.indexOf(langCode) < 0){
		langCode = 'en';
	}


	$('#error_password').hide();
	$('#error_password2').hide();
	$('#error_new_password').hide();
	$('#error_new_password2').hide();
	$('#error_confirm_password').hide();

	$("#newpassword").on("blur keyup",function() {
		var pswd = $(this).val();
		
		var pattern = /^.*(?=.{8,16})(?=.*[0-9])(?=.*[a-zA-Z]).*$/;

		if(!pattern.test(pswd)) {
			//alert("비밀번호는 8자리 이상 문자, 숫자, 특수문자로 구성하여야 합니다."); 
			$('#error_password2').show();
		}else { 
			$('#error_password2').hide();
		} 
	});
	$("#passwordConfirm").on("keyup blur",function() {
		var pswd = $("#newpassword").val();
		var pswdChk = $(this).val();
		
		if(pswd === pswdChk){
			$('#error_confirm_password').hide();
		}else{
		//	$('#passwordConfirm').focus();
			$('#error_confirm_password').show();
		}
		//if( pswd.length > 0)$('#error_passwordConfirm').hide();
	});
	$("#mypage6Submit").on("click",function (event) {
		
		//인증번호 체크
		if($('#password').val().length == 0){
            $('#error_password').show();
            $('#password').val('');
            $('#password').focus();
			return false;
		}

		var pswd = $('#newpassword').val();
	    
		var pattern = /^.*(?=.{8,16})(?=.*[0-9])(?=.*[a-zA-Z]).*$/;

		if(!pattern.test(pswd)) {
	    	alert(eval('msgP.'+langCode+'.passwdChk'));
	    	$('#newpassword').focus();
	    	return false; 
	    }
	    
		if($('#newpassword').val().length == 0){
            $('#error_new_password').show();
            $('#newpassword').val('');
            $('#newpassword').focus();
			return false;
		}

		if($('#passwordConfirm').val().length == 0){
            $('#error_new_password2').show();
            $('#passwordConfirm').val('');
            $('#passwordConfirm').focus();
			return false;
		}
		
		//비밀번호 비교체크
		var p = $('#newpassword').val();
		var pConfirm  = $('#passwordConfirm').val();
		if(p != pConfirm){
            $('#error_confirm_password').show();
            $('#passwordConfirm').val('');
            $('#passwordConfirm').focus();
			return false;
		}

		$('#myform').attr('action','/mypage/pdChangeAction');
		document.myform.submit();
	});


});