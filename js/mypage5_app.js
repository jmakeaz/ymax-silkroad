$(document).ready(function() {
	var langCode = location.pathname.split('/')[1];	
	var langArray = "en|ko|cn|jp";
	if(langCode == null || langArray.indexOf(langCode) < 0){
		langCode = 'en';
	}


	$('#success_sms').hide();  
	$('#error_sms').hide();
	$('#infoAuth').hide(); 
	$('#smsReSend').hide(); 

	//SMS 인증 Action
	$("#mypage5SmsSubmit").on("click",function (event) {
		
		if($("input[name=country_code]").val() == ''){
			alert(eval('msgP.'+langCode+'.nationCd'));				
			return false;
		}
		
		//휴대폰번호 체크
		if($('#phone_number').val().length == 0){
			alert(eval('msgP.'+langCode+'.insMobile'));
            $('#phone_number').val('');
            $('#phone_number').focus();
			return false;
		}
		//노출 info 초기화
		$('#error_sms').hide();
		$('#success_sms').hide();
		$('#infoAuth').hide();  


		$('#smsReSend').show();  
		
		//csrf 설정
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");	
		
		var search = {}
	    search["change"] = 1;
	    search["email"] = $("#username").val();
	    search["phoneNumber"] = $("#phone_number").val();
	    search["countryCode"] = $("input[name=country_code]").val();

		//alert(JSON.stringify(search));

		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: "/SMS/sendSMSAuth",
			data: JSON.stringify(search),
			dataType: 'json',
			cache: false,
			timeout: 600000,
			beforeSend: function( xhr ) {
			    xhr.setRequestHeader(header, token);
			},
			success: function (data) {
					$('#success_sms').show();
					//$('#infoAuth').show();  
			},
			error: function (e) {
				var errorData = JSON.parse(JSON.stringify(e));
				if(errorData.responseText == "success"){
					$('#success_sms').show();
					//$('#infoAuth').show();  
				}else{
					$('#error_sms').show();
				}

			}
		});
	
	});

	$("#smsReSend").on("click",function(event){
		$("#mypage5SmsSubmit").click();
	});

	$("#mypage5submit").on("click",function (event) {

		//국가코드 체크
		if($('input[name=country_code]').val()==''){
			//alert('국가코드를 선택해주십시요.' + $('input[name=country_code]').val());
			alert(eval('msgP.'+langCode+'.nationCd') + $('input[name=country_code]').val());
            $('input[name=country_code]').val('');
            $('input[name=country_code]').focus();
			return false;
		}
		
		//휴대폰번호 체크
		if($('#phone_number').val().length == 0){
			//alert('휴대폰 번호를 입력해 주십시요.');
			alert(eval('msgP.'+langCode+'.insMobile'));
            $('#phone_number').val('');
            $('#phone_number').focus();
			return false;
		}
		
		//인증번호 체크
		if($('#authCode').val().length == 0){
			//alert('인증번호를 입력해 주십시요.');
			alert(eval('msgP.'+langCode+'.insAuthcode'));
            $('#authCode').val('');
            $('#authCode').focus();
			return false;
		}

		$('#myform').attr('action','/mypage/infoChangeAction');
		document.myform.submit();
	});



});



// 숫자만 입력가능하게 한다.
function onlyNumber() {
	var oElement = (arguments[0] != null) ? arguments[0] : this;
	var charChk;

	for(var i=0; i<oElement.value.length; i++){
		charChk = oElement.value.charCodeAt(i);

		if(charChk > 57 || charChk < 48){
			//alert("공백없이 숫자로만 입력해주세요.");
			oElement.value = oElement.value.substring(0, i);
			oElement.focus();
			return;
		}
	}
}