

Number.prototype.to2 = function() { return (this > 9 ? "" : "0")+this; };
Date.prototype.getDateString = function(dateFormat) {
	var result = "";
	
	dateFormat = dateFormat == 8 && "YYYY.MM.DD" || dateFormat == 6 && "hh:mm:ss" || dateFormat || "YYYY.MM.DD hh:mm:ss";
	for (var i = 0; i < dateFormat.length; i++) {
		result += dateFormat.indexOf("YYYY", i) == i ? (i+=3, this.getFullYear()                     ) :
		dateFormat.indexOf("YY",   i) == i ? (i+=1, String(this.getFullYear()).substring(2)) :
		dateFormat.indexOf("MM",   i) == i ? (i+=1, (this.getMonth()+1).to2()              ) :
		dateFormat.indexOf("M",    i) == i ? (      this.getMonth()+1                      ) :
		dateFormat.indexOf("DD",   i) == i ? (i+=1, this.getDate().to2()                   ) :
		dateFormat.indexOf("D"   , i) == i ? (      this.getDate()                         ) :
		dateFormat.indexOf("hh",   i) == i ? (i+=1, this.getHours().to2()                  ) :
		dateFormat.indexOf("h",    i) == i ? (      this.getHours()                        ) :
		dateFormat.indexOf("mm",   i) == i ? (i+=1, this.getMinutes().to2()                ) :
		dateFormat.indexOf("m",    i) == i ? (      this.getMinutes()                      ) :
		dateFormat.indexOf("ss",   i) == i ? (i+=1, this.getSeconds().to2()                ) :
		dateFormat.indexOf("s",    i) == i ? (      this.getSeconds()                      ) :
		(dateFormat.charAt(i)                         ) ;
	}
	return result;
}; 


function dateAdd(date, interval, units) {
	var ret = new Date(date); //don't change original date
	var checkRollover = function() { if(ret.getDate() != date.getDate()) ret.setDate(0);};
	switch(interval.toLowerCase()) {
		case 'year'   :  ret.setFullYear(ret.getFullYear() + units); checkRollover();  break;
		case 'quarter':  ret.setMonth(ret.getMonth() + 3*units); checkRollover();  break;
		case 'month'  :  ret.setMonth(ret.getMonth() + units); checkRollover();  break;
		case 'week'   :  ret.setDate(ret.getDate() + 7*units);  break;
		case 'day'    :  ret.setDate(ret.getDate() + units);  break;
		case 'hour'   :  ret.setTime(ret.getTime() + units*3600000);  break;
		case 'minute' :  ret.setTime(ret.getTime() + units*60000);  break;
		case 'second' :  ret.setTime(ret.getTime() + units*1000);  break;
		default       :  ret = undefined;  break;
	}
	return ret;
}


function getLocalDate(yyy, mmm, ddd, hhh, mmmm, formatStr){
	//console.log(yyy+":"+mmm+":"+ddd+":"+hhh+":"+mmmm);
	var date1 = new Date(Number(yyy), Number(mmm)-1,Number(ddd),Number(hhh),Number(mmmm)); 
	
	var format = "YYYY.MM.DD hh:mm";
	if(formatStr!=null && formatStr!=""){
		format = formatStr;
	}
	
	var labourDay = new Date();
	var labourDayOffset = (labourDay.getTimezoneOffset() / 60);
	
	//console.log("labourDayOffset."+labourDayOffset) 
	
	var redate = dateAdd(date1, 'hour', (labourDayOffset)*-1);
	
	return redate.getDateString(format);
}


