// common function
$(document).ready(function () {
    if ($('body').has('.page-main').length) {
        $('.gnb-list>li').each(function () { // gnb 메뉴 슬라이드 액션
            $(this).find('a').on('click', function () {
                var url = $(this).attr('href');
                var sectionId = url.split('#')[1];

                if (sectionId === undefined) {
                    return;
                } else {
                    $('html').animate({
                        scrollTop: $('#' + sectionId).offset().top
                    }, 800);
                }
            });
        });
    }

    $('.button-top').on('click', function () {
        $('html, body').animate({
            scrollTop: $('body').offset().top + 'px'
        }, 800);
    });

    // placeholder가 적용 안되는 브라우저에만
    if ($('html').hasClass('no-placeholder')) {
        $("input").each(function () {
            var ipt = $(this);
            if (ipt.val()) {
                return;
            }
            if (ipt.attr("placeholder")) {
                var spTag;

                if (ipt.attr("class") == undefined) {
                    spTag = '<p class="hiddenInputWrap"></p>';
                } else {
                    spTag = '<p class="hiddenInputWrap ' + ipt.attr("class") + '"></p>';
                }

                ipt.wrap(spTag).after('<span class="hiddenInput">' + ipt.attr("placeholder") + "</span>");

                ipt.focusin(function () {
                    $(this).next(".hiddenInput").hide();
                });
                ipt.parent().find(".hiddenInput").on("click", function () {
                    $(this).hide().prev().focus();
                });
                ipt.focusout(function () {
                    if ($(this).val()) {
                        return false;
                    } else {
                        $(this).next().show();
                    }
                });
            }
        });
    }

    // language select js : s
    var $langSelect = $(".js-lang-select");
    var $langSelectOption = $langSelect.find(".d-select-option");

    $langSelect.click(function (e) {
        $(this).addClass("active");
        e.stopPropagation();
    });

    $langSelectOption.click(function (e) {
        var _this = $(this);
        var dataValue = _this.data("value");
        location.href = "/" + dataValue + "/";
    });

    $('body').click(function (e) {
        if ($langSelect.hasClass('active')) {
            if (!$langSelect.is(e.target) && $langSelect.has(e.target).length === 0) {
                $langSelect.removeClass('active');
            }
        }
    });
    $(window).scroll(function () {
        if ($('body').hasClass('header-fixed') && $('.js-lang-select').hasClass('active')) {
            $('.js-lang-select').removeClass('active');
        }
    });
    // language select js : e

    // 회원가입 전체동의 : s
    var $agreeItems = $('#check01, #check02, #check03');
    var $allAgreeItem = $('#checkAll');
    $allAgreeItem.change(function () {
        if ($(this).is(':checked')) {
            $agreeItems.prop('checked', true);
        } else {
            $agreeItems.prop('checked', false);
        }
    });
    $agreeItems.change(function () {
        var checkedLength = 0;
        $agreeItems.each(function () {
            if ($(this).prop('checked')) {
                checkedLength += 1;
            }
        });
        if (checkedLength == $agreeItems.length) {
            $allAgreeItem.prop('checked', true);
        } else {
            $allAgreeItem.prop('checked', false);
        }
    });
    // 회원가입 전체동의 : e
});


/* ========================================================== */

// function list(layout)
function headerFixed() { // heaeder
    var $body = $("body");

    // 메인과 프리세일 페이지에만 top banner가 추가
    if ($('body').has($('.page-main')).length && $('.top-banner').is(':visible')) {
        headerFixedToggle(313);
    } else if ($('body').has($('.page-sales')).length && $('.top-banner').is(':visible')) {
        headerFixedToggle(156);
    } else {
        headerFixedToggle(86);
    }
}

function headerFixedToggle(height) {
    var $body = $("body");
    if ($(window).scrollTop() > height) {
        $body.addClass("header-fixed");
    } else {
        $body.removeClass("header-fixed");
    }
}

function footerFixed() { // footer
    var $body = $("body");
    var headerH = $body.find(".header").outerHeight();
    var containerH = $body.find(".container").outerHeight();
    var footerH = $body.find(".footer").outerHeight();
    var total = headerH + containerH + footerH;
    if (total < $(window).height()) {
        $body.addClass("footer-fixed");
    } else {
        $body.removeClass("footer-fixed");
    }
}


/* ========================================================== */


// function list (main)
function toTopFixed() {
    if ($('body').has('.button-top').length) {
        var footerTop = $('.footer').position().top;
        var scrollTop = $(document).scrollTop() + window.innerHeight;
        var difference = scrollTop - footerTop + 20;
        var $topButton = $('.button-top');
        var $quickSns = $('.quickSns');

        if ($(document).scrollTop() < 800) {
            $topButton.hide();
        } else {
            $topButton.show();
        }

        if (scrollTop > footerTop) {
            $topButton.css({
                'bottom': difference
            });
            $quickSns.css({
                'bottom': difference
            });
        } else {
            $topButton.css({
                'bottom': 20
            });
            $quickSns.css({
                'bottom': 92
            });
        }
    }
}

// 서브페이지(프레스룸, 마이페이지)에서 gnb active
function subpageGnbActive() {
    var $gnbList = $('.gnb-list');
    var url = window.location.href;
    var $mypageMenu = $('.state-login a:first-child');

    // Press room
    var pressReg = /\/press\//;

    // sales room
    var salesReg = /\/sale/;

    // My page
    var mypageReg = /\/mypage\//;

    // My page > 토큰 구매하기
    var mypageBuyReg = /\/mypage\/buy/;

    $mypageMenu.removeClass('active');

    if ($('body').hasClass('.page-main')) {
        return false;
    } else {
        $gnbList.find('li').removeClass('active');
        if (pressReg.test(url)) {
            $gnbList.find('li').eq('7').addClass('active');
            return;
        }
        if (mypageReg.test(url)) {
            $mypageMenu.addClass('active');
        }
        if (mypageBuyReg.test(url) || salesReg.test(url)) {
            $gnbList.find('li').eq('8').addClass('active');
        }
    }
}

function scrollGnbActive() {
    var $gnbList = $('.gnb-list');

    if (!$('body').has('.page-main').length) {
        return false;
    }
    $('.page-main .section').each(function (index) {
        var $this = $(this);
        var sectionId = $this.attr('id');
        if ($this.position().top <= $(window).scrollTop()) {
            $gnbList.find('li').removeClass('active');
            if (!sectionId) {
                $gnbList.find('li').eq(index - 1).addClass('active');
            } else {
                $gnbList.find('a[href*=' + sectionId + ']').parent().addClass('active');
            }
        }
        if ($(document).height() - $(window).height() === $(window).scrollTop()) { // 모바일지갑
            $gnbList.find('li').removeClass('active');
            $gnbList.find('a[href*=mainBanner]').parent().addClass('active');
        }
    });
}

function sbh() { // service benefit height 통일
    var sbh = $('.service-benefit').height();
    $('.service-benefit').find('>div').each(function () {
        $(this).css('height', sbh);
    });
}
$(document).ready(function () {
    $(".main-slider").slick({
        autoplay: true,
        autoplaySpeed: 2000,
        dots: false,
        arrows: false,
        infinite: true,
        speed: 2000,
        fade: true,
        cssEase: "linear"
    });

    // main visual animate
    function sd() {
        $('.scroll-down ').animate({
            'margin-top': '82px'
        }, 600, function () {
            $('.scroll-down').animate({
                'margin-top': '90px'
            }, 600);
            sd();
        });
    }
    sd();

    // team profile function
    $('.profile-more-button').click(function (e) {
        var _this = $(this);
        if (!_this.hasClass('show')) {
            _this.addClass('show').text('Hide').prev('.profile-story ').slideDown();
        } else {
            _this.removeClass('show').text('Read more').prev('.profile-story ').slideUp();
        }
    });

    // faq function
    $('.faq-question a').click(function (e) {
        e.preventDefault();
        var _this = $(this);
        if (!_this.closest('.faq-item').hasClass('active')) {
            _this.closest('.faq-item').addClass('active');
        } else {
            _this.closest('.faq-item').removeClass('active');
        }
    });
});

$('.jsCloseTopBanner').click(function () {
    var $this = $(this);
    $this.parents('.top-banner').slideUp('200');
});



/* ========================================================== */



// function list (content)
$(".js-close-popup").click(function () { // my page popup close
    $(this).parents(".popup").hide();
});



/* ========================================================== */


// 실행
$(window).load(function () {
    scrollGnbActive();
    subpageGnbActive();
    headerFixed();
    toTopFixed();
    footerFixed();
    sbh();
});

$(window).scroll(function () {
    headerFixed();
    toTopFixed();
    scrollGnbActive();
});

$(window).resize(function () {
    headerFixed();
    footerFixed();
});




$(document).ready(function () {
    $('.quickSns li').each(function () {
        $(this).find('a').on('mouseover', function () {
            $(this).find('img').attr('src', $(this).find('img').attr('src').replace('_off', '_on'));
        })
        $(this).find('a').on('mouseout', function () {
            $(this).find('img').attr('src', $(this).find('img').attr('src').replace('_on', '_off'));
        })
    })
    $('.quickSns>a').on('mouseover', function () {
        if ($(this).hasClass('open')) {
            return false;
        } else {
            $(this).find('img').attr('src', $(this).find('img').attr('src').replace('_off', '_over'))
        }
    })
    $('.quickSns>a').on('mouseout', function () {
        if ($(this).hasClass('open')) {
            return false;
        } else {
            $(this).find('img').attr('src', $(this).find('img').attr('src').replace('_over', '_off'))
        }
    })
    $('.quickSns>a').on('click', function () {
        $(this).toggleClass('open').prev('ul').slideToggle();
        if ($(this).hasClass('open')) {
            $(this).find('img').attr('src', $(this).find('img').attr('src').replace('_over', '_on'))
        } else {
            $(this).find('img').attr('src', $(this).find('img').attr('src').replace('_on', '_over'))
        }
        return false;
    })
})