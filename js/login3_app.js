
//var recaptchaChecked = false;

$(document).ready(function() {
	var langCode = location.pathname.split('/')[1];	
	var langArray = "en|ko|cn|jp";
	if(langCode == null || langArray.indexOf(langCode) < 0){
		langCode = 'en';
	}

	$('#lv4').hide();
	$('#lv3').hide();
	$('#lv2').hide();
	$('#lv1').hide();
	$('#error_password2').hide();
	$('#error_passwordConfirm').hide();

	$("#password").on("blur keyup",function() {
		var pswd = $(this).val();
		
		var pattern = /^.*(?=.{8,16})(?=.*[0-9])(?=.*[a-zA-Z]).*$/;

		if(!pattern.test(pswd)) {
			//alert("비밀번호는 8자리 이상 문자, 숫자, 특수문자로 구성하여야 합니다."); 
			$('#lv1').hide();
			$('#lv4').show();
			$('#error_password2').show();
			return false; 
		}else { 
			$('#lv4').hide();
			$('#error_password2').hide();
			$('#lv1').show();
			return true; 
		} 
	});

	$("#passwordConfirm").on("keyup blur",function() {
		var pswd = $("#password").val();
		var pswdChk = $(this).val();
		
		if(pswd === pswdChk){
			$('#error_passwordConfirm').hide();
		}else{
		//	$('#passwordConfirm').focus();
			$('#error_passwordConfirm').show();
		}
	});
	//로그인 Submit 액션
	$("#login3submit1").on("click",function (event) {    
		
		var pswd = $('#password').val();
	    
		var pattern = /^.*(?=.{8,16})(?=.*[0-9])(?=.*[a-zA-Z]).*$/;

		if(!pattern.test(pswd)) {
	    	alert(eval('msgP.'+langCode+'.passwdChk'));
	    	$('#password').focus();
	    	return false; 
	    }
		//비밀번호 체크
		var p = $('#password').val();
		var pConfirm  = $('#passwordConfirm').val();
		if(p != pConfirm){
			$('#error_passwordConfirm').show();
            $('#passwordConfirm').val('');
            $('#passwordConfirm').focus();
			return false;
		}

		//구글 체크?
		/*
		if(!recaptchaChecked){
			alert(eval('msgP.'+langCode+'.chkRecaptcha'));
			return false;
		}
		*/
		$('#myform').attr('action','/'+langCode+'/User/pwComplete');
		document.myform.submit();

    });


});


function recaptchaCallback() {
	recaptchaChecked = true;
};
