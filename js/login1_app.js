
//var recaptchaChecked = false;

$(document).ready(function() {
	var langCode = location.pathname.split('/')[1];	
	var langArray = "en|ko|cn|jp";
	if(langCode == null || langArray.indexOf(langCode) < 0){
		langCode = 'en';
	}

	$("#username").keyup(function() {
		//한글 안됨
		var regex = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/g;
		var email = $('#username').val();
		if (regex.test(email) === true) {
			$('#username').val('');
            $('#username').focus();
			return false;
		}else{
		}
	});
	//로그인 Submit 액션
	$("#login1submit1").on("click",function (event) {    
		//Email 체크
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		var email = $('#username').val();
		if (regex.test(email) === false) {
            $('#username').val('');
            $('#username').focus();
			return false;
        }
		/*
		//구글 체크?
		if(!recaptchaChecked){
			alert(eval('msgP.'+langCode+'.chkRecaptcha'));
			return false;
		}
		*/

		$('#myform').attr('action','/User/login');
		document.myform.submit();

    });


});


function recaptchaCallback() {
	recaptchaChecked = true;
};
