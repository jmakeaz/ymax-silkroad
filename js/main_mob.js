$(document).ready(function () {
    var mob_gnb;
    mob_gnb = '<div class="mob_gnb"><div class="mgInner">';
    mob_gnb += '	<div class="mob_gnbTop">';
    // mob_gnb += '		<div class="lang-select-wrap">' + $('.header-language').html() + '</div>';
    mob_gnb += '		<div class="lang-select-wrap"></div>';
    mob_gnb += '		<a href="' + $(".logo>a").attr("href") + '" class="mob_gnbClose"><img src="https://d3ly1h878gyzeb.cloudfront.net/img/mobLogo.png" alt=""/></a>';
    mob_gnb += '		<a href="javascript:;" class="mobGnbCloseBt"><img src="https://d3ly1h878gyzeb.cloudfront.net/img/mobMenuClose.png" alt=""/></a>';
    mob_gnb += '	</div>';
    mob_gnb += '	<div class="mob_user">' + $('.header-user-area').html() + '</div>';
    mob_gnb += '	<ul class="mob_gnbList">' + $(".gnb-list").html() + '</ul>';
	mob_gnb += '	<div class="mob_sns">' + $('.sns_group').html() + '</div>';
    mob_gnb += '</div></div>';

    $('#header').append(mob_gnb).append('<a href="javascript:;" class="mobGnbBt"><span></span><span></span><span></span></a>');
	$('.mob_sns a').each(function(){
		$(this).find('img').attr('src',$(this).find('img').attr('src').replace('.png','_bk.png'))
	})

    if ($('body').has('.page-main').length) {
        $('.mob_gnbList>li').each(function () { // gnb 메뉴 슬라이드 액션
            $(this).find('>a').on('click', function () {
                var gnbArr = $(this).attr('href').split('#');
                if ($(this).text() == 'PRESSROOM' || $(this).text() == 'BUY TOKENS') {
                    return;
                } else {
                    mGnbPage();
                    $('html, body').animate({
                        scrollTop: $('#' + gnbArr[1]).offset().top + 'px'
                    }, 800);
                }
            });
        });
    }

    setHeaderPosition();

    //메뉴 열기 & 닫기
    var scrollHeight = '0';
    $('.mobGnbBt').on('click', function () {
        scrollHeight = $(window).scrollTop();
        $('body').css({
            'position': 'fixed',
            'top': '-' + scrollHeight + 'px'
        })
        $('.mobGnbBt').fadeOut();
        $('.mob_gnb').animate({
            'opacity': '1',
            'top': '0'
        }, 300);
    });

    function mGnbPage() {
        $('body').css({
            'position': 'static'
        })
        $('.mobGnbBt').fadeIn();
        $('.mob_gnb').animate({
            'opacity': '0',
            'top': '-100%'
        }, 300);
        $('html, body').animate({
            scrollTop: scrollHeight + 'px'
        }, 10);
    }
    $('.mobGnbCloseBt').on('click', function () {
        mGnbPage();
    });

    // main > home 높이값 지정
    // setMainHomeHeight();

    // main > home 배경 슬라이드
    $(".main-slider").slick({
        autoplay: true,
        autoplaySpeed: 2000,
        dots: false,
        arrows: false,
        infinite: true,
        speed: 2000,
        fade: true,
        cssEase: "linear"
    });

    // 로그인, 회원가입 header: 언어선택 셀렉트박스
    var mLangSelect = '';
    $('.js-lang-select .d-select-option').each(function (i, el) {
        var $this = $(this);
        mLangSelect += '<option value="' + $this.data('value') + '">' + $this.text() + '</option>';
    });

    // main 페이지와 login 페이지의 header가 다르므로 따로 추가
    if ($('body').has('.header-login').length) {
        $('.header-language').append('<select id="mLangSelect">' + mLangSelect + '</select>');
    } else {
        $('.lang-select-wrap').append('<select id="mLangSelect">' + mLangSelect + '</select>');
    }

    // language selectbox selected default
    var bodyLangClass = $('body').attr('class').split(' ').filter(function (item) {
        return !item.indexOf('lang_');
    });
    var langClass = bodyLangClass[0].split('_');
    $('#mLangSelect').val(langClass[1]);

    // 로그인, 회원가입 header: 언어선택 셀렉트박스 변경되었을 때
    $('#mLangSelect').on('change', function () {
        location.href = "/" + $(this).val() + "/";
    });
});

// main > team profile function
$('.profile-more-button').click(function (e) {
    var _this = $(this);
    if (!_this.hasClass('show')) {
        _this.addClass('show').text('Hide').prev('.profile-story ').slideDown();
    } else {
        _this.removeClass('show').text('Read more').prev('.profile-story ').slideUp();
    }
});

// main > FAQ toggle
$('.faq-question a').click(function (e) {
    e.preventDefault();
    var _this = $(this);
    if (!_this.closest('.faq-item').hasClass('active')) {
        _this.closest('.faq-item').addClass('active');
    } else {
        _this.closest('.faq-item').removeClass('active');
    }
});

// 서브페이지(프레스룸, 마이페이지)에서 gnb active
function subpageGnbActive() {
    var $gnbList = $('.mob_gnbList');
    var $mypageMenu = $('.state-login a:first-child');
    var url = window.location.href;

    // Press room
    var pressReg = /\/press\//;

    // My page
    var mypageReg = /\/mypage\//;

    // My page > 토큰 구매하기
    var mypageBuyReg = /\/mypage\/buy/;

    $mypageMenu.removeClass('active');

    if (!$('body').has('.page-main').length) {

        $gnbList.find('li').removeClass('active');

        if (pressReg.test(url)) {
            $gnbList.find('li').eq('7').addClass('active');
            return;
        }
        if (mypageReg.test(url)) {
            $mypageMenu.addClass('active');
        }
        if (mypageBuyReg.test(url)) {
            $gnbList.find('li').eq('8').addClass('active');
        }
    }
}

// main > 스크롤 내릴 때 gnb 메뉴 활성화
function scrollGnbActive() {
    var $gnbList = $('.mob_gnbList');
    if (!$('body').has('.page-main').length) {
        return false;
    }
    $('.page-main .section').each(function (index) {
        var $this = $(this);
        var sectionId = $this.attr('id');
        if ($this.position().top <= $(window).scrollTop()) {
            $gnbList.find('li').removeClass('active');
            if (!sectionId) {
                $gnbList.find('li').eq(index - 1).addClass('active');
            } else {
                $gnbList.find('a[href*=' + sectionId + ']').parent().addClass('active');
            }
        }
        if ($(document).height() - $(window).height() === $(window).scrollTop()) { // 모바일지갑
            $gnbList.find('li').removeClass('active');
            $gnbList.find('a[href*=mainBanner]').parent().addClass('active');
        }
    });
}

// main > home 높이값 지정
// function setMainHomeHeight() {
//     if (!$('body').has('.page-main').length) {
//         return false;
//     }
//     var viewHeight = window.visualViewport.height;
//     var mainHomeMinHeight = 500;
//     var headerHeight = 56;
//     if (viewHeight > 500) {
//         $('.main-home, .main-home-bg').height(viewHeight - headerHeight);
//     } else {
//         $('.main-home, .main-home-bg').removeAttr('style');
//     }
// }

// 실행
$(window).load(function () {
    scrollGnbActive();
    subpageGnbActive();
});

$(window).scroll(function () {
    scrollGnbActive();
    setHeaderPosition();
});

// $(window).resize(function () {
//     setMainHomeHeight();
// });

function setHeaderPosition() {
    var $header = $('.header');
    var $menuButton = $header.find('.mobGnbBt');
    var $container = $header.next('.container');
    console.log($('.top-banner').is(':visible'));
    if ($('body').has($('.page-main')).length) {
        if ($(window).scrollTop() < 150 && $('.top-banner').is(':visible')) {
            $header.css('position', 'static');
            $container.css('paddingTop', 0);
            $menuButton.css('top', 161);
        } else {
            $header.css('position', 'fixed');
            $container.css('paddingTop', 56);
            $menuButton.css('top', 11);
        }
    }

    if ($('body').has($('.page-sales')).length && $('.top-banner').is(':visible')) {
        if ($(window).scrollTop() < 40) {
            $header.css('position', 'static');
            $container.css('paddingTop', 0);
            $menuButton.css('top', 51);
        } else {
            $header.css('position', 'fixed');
            $container.css('paddingTop', 56);
            $menuButton.css('top', 11);
        }
    }
}

$('.jsCloseTopBanner').click(function () {
    var $this = $(this);
    $this.parents('.top-banner').slideUp('200', function () {
        $('.header').css('position', 'fixed');
        $('.container').css('paddingTop', 56);
    });

    $('.mobGnbBt').animate({
        top: 11
    }, 400);
});