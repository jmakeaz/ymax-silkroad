# Silkroad

## 구조

- css
    - main.css(모든 css 파일이 import 되어 있다.)
    - base.css(reset, helper class)
    - components.css(form, button, table, tab...)
    - layout.css(header, footer, body style)
    - mypage.css(마이페이지)
- font
- html
    - main
        - index.html
    - mypage
        - wallet.html: 토큰지갑
        - buy.html: 토큰 구매하기
        - history.html: 거래내역(ETH)
        - historySLK.html:거래내역(SLK)
        - myInfoModify.html: 개인정보 변경
        - changePassword.html: 비밀번호 변경
- img
    - cn
    - en
    - jp
    - ko
    - vn
    - email
    - teams
- js
    - vendor