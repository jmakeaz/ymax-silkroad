
$(document).ready(function() {

	var imgSrc = $('html').find('img').eq(0).attr('src');
	var langCode = imgSrc.substring(imgSrc.indexOf('.net')+5, imgSrc.indexOf('/images'));
	var langArray = "en|ko|ru|jp|cn";
	if(langCode == null || langArray.indexOf(langCode) < 0){
		langCode = 'ko';
	}
	$('#sms1reSend').hide();
	$('#infoAuth').hide();

    //code here
	$(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
     $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

	 $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

	//Sms 인증번호 액션
	$("#sms1SmsSubmit").on("click",function (event) {
		$('#sms1reSend').show();  
		
		//csrf 설정
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");	
		
		var search = {}
	    search["change"] = 0;
	    search["email"] = $("#username").val();
	    search["phoneNumber"] = $("#phone_number").val();
	    search["countryCode"] = $("input[name=country_code]").val();

		//alert(JSON.stringify(search));

		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: "/SMS/sendSMSAuth",
			data: JSON.stringify(search),
			dataType: 'json',
			cache: false,
			timeout: 600000,
			beforeSend: function( xhr ) {
			    xhr.setRequestHeader(header, token);
			},
			success: function (data) {
				$('#success_auth').show();
				$('#infoAuth').show();  
			},
			error: function (e) {
				var errorData = JSON.parse(JSON.stringify(e));
				if(errorData.responseText == "success"){
					$('#success_auth').show();
					$('#infoAuth').show();  
				}else if(errorData.responseText == "nomatch"){
					alert(eval('msgP.'+langCode+'.notResistM'));
					//alert('등록 된 전화번호가 아닙니다.');
				}else{
					$('#error_auth').show();
				}

			}
		});
	
	});

	$("#sms1reSend").on("click",function(event){
		$("#sms1SmsSubmit").click();
	});


	//sms인증 Submit 액션
	$("#sms1submit").on("click",function (event) {    

		//국가코드 체크
		if($('input[name=country_code]').val()==''){
			//alert('국가코드를 선택해주십시요.' + $('input[name=country_code]').val());
			alert(eval('msgP.'+langCode+'.nationCd') + $('input[name=country_code]').val());
            $('input[name=country_code]').val('');
            $('input[name=country_code]').focus();
			return false;
		}
		
		//휴대폰번호 체크
		if($('#phone_number').val().length == 0){
			//alert('휴대폰 번호를 입력해 주십시요.');
			alert(eval('msgP.'+langCode+'.insMobile'));
            $('#phone_number').val('');
            $('#phone_number').focus();
			return false;
		}
		
		//인증번호 체크
		if($('#authCode').val().length == 0){
			//alert('인증번호를 입력해 주십시요.');
			alert(eval('msgP.'+langCode+'.insAuthcode'));
            $('#authCode').val('');
            $('#authCode').focus();
			return false;
		}

		$('#myform').attr('action','/Mypage/smsFormAction').submit();

    });


});