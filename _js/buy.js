
$(document).ready(function() {
	
	$.get("https://crix-api-endpoint.upbit.com/v1/crix/candles/days?code=CRIX.UPBIT.KRW-ETH", function(data){
			var pData =JSON.stringify(data);
			var parseData = JSON.parse(pData.replace("[", "").replace("]", "").toString());

			var tradePrice = parseData.tradePrice; //현재가
			var highPrice = parseData.highPrice; //고가
			var lowPrice = parseData.lowPrice; //저가
			var change = parseData.change; //FALL : 하락 RISE : 상승
			var changePrice = parseData.changePrice; //전일대비 변동금액 
			var signedChangePrice = parseData.signedChangePrice; //전일대비 변동금액 Singed
			var signedChangeRate = parseData.signedChangeRate; //전일대비 변동 %

			var pmString = "+";
				if(parseInt(signedChangePrice,0) < 0)
					pmString = "-";
			
			var parsRate = parseFloat(signedChangeRate) * 100;
			
			$('#tradePrice').text(comma(tradePrice));
			$('#highPrice').text(comma(highPrice));
			$('#lowPrice').text(comma(lowPrice));
			$('#signedChangePrice').text(pmString + comma(changePrice));
			$('#signedChangeRate').text(parsRate.toFixed(2)+"%");
			
			if($('.krw_wrap').find('.up'))
				$('.krw_wrap').removeClass("up");

			if($('.krw_wrap').find('.down'))
				$('.krw_wrap').removeClass("down");

			if(change=="RISE")
				$('.krw_wrap').addClass("up");
			else 
				$('.krw_wrap').addClass("down");
		});

	setInterval(function(){
		$.get("https://crix-api-endpoint.upbit.com/v1/crix/candles/days?code=CRIX.UPBIT.KRW-ETH", function(data){
			var pData =JSON.stringify(data);
			var parseData = JSON.parse(pData.replace("[", "").replace("]", "").toString());

			var tradePrice = parseData.tradePrice; //현재가
			var highPrice = parseData.highPrice; //고가
			var lowPrice = parseData.lowPrice; //저가
			var change = parseData.change; //FALL : 하락 RISE : 상승
			var changePrice = parseData.changePrice; //전일대비 변동금액 
			var signedChangePrice = parseData.signedChangePrice; //전일대비 변동금액 Singed
			var signedChangeRate = parseData.signedChangeRate; //전일대비 변동 %

			var pmString = "+";
				if(parseInt(signedChangePrice,0) < 0)
					pmString = "-";
			
			var parsRate = parseFloat(signedChangeRate) * 100;
			
			$('#tradePrice').text(comma(tradePrice));
			$('#highPrice').text(comma(highPrice));
			$('#lowPrice').text(comma(lowPrice));
			$('#signedChangePrice').text(pmString + comma(changePrice));
			$('#signedChangeRate').text(parsRate.toFixed(2)+"%");
			
			if($('.krw_wrap').find('.up'))
				$('.krw_wrap').removeClass("up");

			if($('.krw_wrap').find('.down'))
				$('.krw_wrap').removeClass("down");

			if(change=="RISE")
				$('.krw_wrap').addClass("up");
			else 
				$('.krw_wrap').addClass("down");
		});
	}, 10000);	

	$('#myetherwallet').click(function (e) {  
            e.preventDefault();  
            var url = "https://myetherwallet.github.io/knowledge-base/getting-started/getting-started-new.html";  
            window.open(url, "_blank");  
        });
		
	$('input[type=radio][name=keyStatus]').change(function() {
		$(".ip_group").toggle();
		toggle = !toggle;
	});
	
	
	
	var imgSrc = $('html').find('img').eq(0).attr('src');
	var langCode = imgSrc.substring(imgSrc.indexOf('.net')+5, imgSrc.indexOf('/images'));
	var langArray = "en|ko|ru|jp|cn";
	if(langCode == null || langArray.indexOf(langCode) < 0){
		langCode = 'ko';
	}

//여기서부터 POPUP 관련
		$('#smsReSendPopUp').hide();  
		$('#success_smsPopUp').hide();
		$('#infoAuthPopUp').hide(); 
		$('#error_smsPopUp').hide(); 

		if(authMode == '1')
			uiPopupSms.init();
		else if(authMode == '2')
			uiPopupOTP.init();

		//SMS 인증 POPUP Action
		$("#mypage5SmsSubmitPopUp").on("click",function (event) {
			$('#smsReSendPopUp').show();  
			
			//csrf 설정
			var token = $("meta[name='_csrf']").attr("content");
			var header = $("meta[name='_csrf_header']").attr("content");	
			
			var search = {}
		    search["change"] = 0;
		    search["email"] = $("#email").val();
		    search["phoneNumber"] = $("#phone_numberPopUp").val();
		    search["countryCode"] = $("input[name=country_code]").val();

			//alert(JSON.stringify(search));

			$.ajax({
				type: "POST",
				contentType: "application/json",
				url: "/SMS/sendSMSAuth",
				data: JSON.stringify(search),
				dataType: 'json',
				cache: false,
				timeout: 600000,
				beforeSend: function( xhr ) {
				    xhr.setRequestHeader(header, token);
				},
				success: function (data) {
						$('#success_smsPopUp').show();
						$('#infoAuthPopUp').show();  
				},
				error: function (e) {
					var errorData = JSON.parse(JSON.stringify(e));
					if(errorData.responseText == "success"){
						$('#success_smsPopUp').show();
						$('#infoAuthPopUp').show();  
					}else{
						$('#error_smsPopUp').show();
					}

				}
			});
		
		});

		$("#smsReSendPopUp").on("click",function(event){
			$("#mypage5SmsSubmitPopUp").click();
		});

		//SMS 인증 확인버튼
		$("#SmsSubmitPopUp").on("click",function(event){
			
			//csrf 설정
			var token = $("meta[name='_csrf']").attr("content");
			var header = $("meta[name='_csrf_header']").attr("content");	
			
			var search = {}
		    search["authCode"] = $("#authCodePopUp").val();
		    search["email"] = $("#email").val();

			//alert(JSON.stringify(search));

			$.ajax({
				type: "POST",
				contentType: "application/json",
				url: "/SMS/verifiedSMSAuth",
				data: JSON.stringify(search),
				dataType: 'json',
				cache: false,
				timeout: 600000,
				beforeSend: function( xhr ) {
				    xhr.setRequestHeader(header, token);
				},
				success: function (data) {
						$('.dimm').hide();
						$('#smsPopUp').hide();
						successAuth();
				},
				error: function (e) {
					var errorData = JSON.parse(JSON.stringify(e));
					if(errorData.responseText == "success"){
						$('.dimm').hide();
						$('#smsPopUp').hide();
						successAuth();
					}else{
						//alert("인증 번호가 틀립니다.");
						alert(eval('msgP.'+langCode+'.incorrectCode'));
					}

				}
			});
		});

		//OTP 인증 POPUP Action
		$("#OtpSubmitPopUp").on("click",function (event) {
			
			//csrf 설정
			var token = $("meta[name='_csrf']").attr("content");
			var header = $("meta[name='_csrf_header']").attr("content");	
			
			var search = {}
		    search["otpCode"] = $("#otpCodePopUp").val();
		    search["email"] = $("#email").val();

			$.ajax({
				type: "POST",
				contentType: "application/json",
				url: "/Mypage/otpVerified",
				data: JSON.stringify(search),
				dataType: 'json',
				cache: false,
				timeout: 600000,
				beforeSend: function( xhr ) {
				    xhr.setRequestHeader(header, token);
				},
				success: function (data) {
					$('#success_auth').show();
					$('#infoAuth').show();  
					successAuth();
				},
				error: function (e) {
					var errorData = JSON.parse(JSON.stringify(e));
					if(errorData.responseText == "success"){
						$('.dimm').hide();
						$('#otpPopUp').hide();
						successAuth();
					}else{
						//alert("OTP 번호가 틀립니다.");
						alert(eval('msgP.'+langCode+'.incorrectOtp'));
					}

				}
			});
		
		});

});

function successAuth(){
	send();
}


window.uiPopupOTP = (function(){
	var el;
	var layerHeight;

	function init(){
		el = $('#otpPopUp');
		layerHeight = el.height() / 2;

		styleFt();
	}

	function styleFt(){
		$('.dimm').show();
		el.show();
		el.css('margin-top',-layerHeight+55);
		//console.log(layerHeight)
	}

	return{init:init}
})();

window.uiPopupSms = (function(){
	var el;
	var layerHeight;

	function init(){
		el = $('#smsPopUp');
		layerHeight = el.height() / 2;
		styleFt();
	}

	function styleFt(){
		$('.dimm').show();
		el.show();
		el.css('margin-top',-layerHeight+55);
		//console.log(layerHeight);
	}

	return{init:init}
})();


window.uiPopupBuy = (function(){
	var el;
	var layerHeight;

	function init(){
		el = $('#buyPopUp');
		$('.dimm').show();
		el.show();
		if (el.height() > $(window).height()){
			el.css({
				'margin-top':'0',
				'overflow':'auto'
			});
		}else{
			el.css({
				'top':'0',
				'margin-top':($(window).height() - el.height())/2+'px'
			});
		}
	}

	//trace(layerHeight)

	return{init:init}
})();

window.uiPopupConfirm = (function(){
	var el;
	var layerHeight;

	function init(){
		el = $('#confirmPopUp');
		layerHeight = el.height() / 2;
		styleFt();
	}

	function styleFt(){
		$('.dimm').show();
		el.show();
		el.css('margin-top',-layerHeight+55);
		//trace(layerHeight)
	}

	return{init:init}
})();
$('#popUpCloseBuy').click(function(){
	$('#buyPopUp').hide();
	$('.dimm').hide();
});