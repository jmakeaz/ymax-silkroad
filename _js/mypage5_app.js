
window.uiPopupOTP = (function(){
	var el;
	var layerHeight;

	function init(){
		el = $('#otpPopUp');
		layerHeight = el.height() / 2;

		styleFt();
	}

	function styleFt(){
		$('.dimm').show();
		el.show();
		el.css('margin-top',-layerHeight+55);
		//console.log(layerHeight)
	}

	return{init:init}
})();

window.uiPopupSms = (function(){
	var el;
	var layerHeight;

	function init(){
		//$("#authy-countries").insertBefore("#phone_numberPopUp");
		$("#countryPopUp").append($("#authy-countries")[0]);
		
		el = $('#smsPopUp');
		layerHeight = el.height() / 2;
		styleFt();
	}

	function styleFt(){
		$('.dimm').show();
		el.show();
		el.css('margin-top',-layerHeight+55);
		//console.log(layerHeight);
	}

	return{init:init}
})();

$(document).ready(function() {
	var imgSrc = $('html').find('img').eq(0).attr('src');
	var langCode = imgSrc.substring(imgSrc.indexOf('.net')+5, imgSrc.indexOf('/images'));
	var langArray = ["en","ko","ru","jp","cn"];
	if(langCode == null || !langArray.includes(langCode)){
		langCode = 'en';
	}	
	$('#success_sms').hide();  
	$('#error_sms').hide();
	$('#infoAuth').hide(); 
	$('#smsReSend').hide(); 

	//SMS 인증 Action
	$("#mypage5SmsSubmit").on("click",function (event) {
		$('#smsReSend').show();  
		
		//csrf 설정
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");	
		
		var search = {}
	    search["change"] = 1;
	    search["email"] = $("#username").val();
	    search["phoneNumber"] = $("#phone_number").val();
	    search["countryCode"] = $("input[name=country_code]").val();

		//alert(JSON.stringify(search));

		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: "/SMS/sendSMSAuth",
			data: JSON.stringify(search),
			dataType: 'json',
			cache: false,
			timeout: 600000,
			beforeSend: function( xhr ) {
			    xhr.setRequestHeader(header, token);
			},
			success: function (data) {
					$('#success_sms').show();
					$('#infoAuth').show();  
			},
			error: function (e) {
				var errorData = JSON.parse(JSON.stringify(e));
				if(errorData.responseText == "success"){
					$('#success_sms').show();
					$('#infoAuth').show();  
				}else{
					$('#error_sms').show();
				}

			}
		});
	
	});

	$("#smsReSend").on("click",function(event){
		$("#mypage5SmsSubmitPopUp").click();
	});

	$("#mypage5submit").on("click",function (event) {

		//국가코드 체크
		if($('input[name=country_code]').val()==''){
			//alert('국가코드를 선택해주십시요.' + $('input[name=country_code]').val());
			alert(eval('msgP.'+langCode+'.nationCd') + $('input[name=country_code]').val());
            $('input[name=country_code]').val('');
            $('input[name=country_code]').focus();
			return false;
		}
		
		//휴대폰번호 체크
		if($('#phone_number').val().length == 0){
			//alert('휴대폰 번호를 입력해 주십시요.');
			alert(eval('msgP.'+langCode+'.insMobile'));
            $('#phone_number').val('');
            $('#phone_number').focus();
			return false;
		}
		
		//인증번호 체크
		if($('#authCode').val().length == 0){
			//alert('인증번호를 입력해 주십시요.');
			alert(eval('msgP.'+langCode+'.insAuthcode'));
            $('#authCode').val('');
            $('#authCode').focus();
			return false;
		}

		$('#myform').attr('action','/Mypage/infoChangeAction').submit();
	});



//여기서부터 POPUP 관련
	$('#smsReSendPopUp').hide();  
	$('#success_smsPopUp').hide();
	$('#infoAuthPopUp').hide(); 
	$('#error_smsPopUp').hide(); 

	if(authMode == '1')
		uiPopupSms.init();
	else if(authMode == '2')
		uiPopupOTP.init();

	//SMS 인증 POPUP Action
	$("#mypage5SmsSubmitPopUp").on("click",function (event) {
		$('#smsReSendPopUp').show();  
		
		//csrf 설정
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");	
		
		var search = {}
	    search["change"] = 0;
	    search["email"] = $("#username").val();
	    search["phoneNumber"] = $("#phone_number").val();
	    search["countryCode"] = $("input[name=country_code]").val();

		//alert(JSON.stringify(search));

		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: "/SMS/sendSMSAuth",
			data: JSON.stringify(search),
			dataType: 'json',
			cache: false,
			timeout: 600000,
			beforeSend: function( xhr ) {
			    xhr.setRequestHeader(header, token);
			},
			success: function (data) {
					$('#success_smsPopUp').show();
					$('#infoAuthPopUp').show();  
			},
			error: function (e) {
				var errorData = JSON.parse(JSON.stringify(e));
				if(errorData.responseText == "success"){
					$('#success_smsPopUp').show();
					$('#infoAuthPopUp').show();  
				}else{
					$('#error_smsPopUp').show();
				}

			}
		});
	
	});

	$("#smsReSendPopUp").on("click",function(event){
		$("#mypage5SmsSubmitPopUp").click();
	});

	//SMS 인증 확인버튼
	$("#SmsSubmitPopUp").on("click",function(event){
		
		//csrf 설정
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");	
		
		var search = {}
	    search["authCode"] = $("#authCodePopUp").val();
	    search["email"] = $("#username").val();

		//alert(JSON.stringify(search));

		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: "/SMS/verifiedSMSAuth",
			data: JSON.stringify(search),
			dataType: 'json',
			cache: false,
			timeout: 600000,
			beforeSend: function( xhr ) {
			    xhr.setRequestHeader(header, token);
			},
			success: function (data) {
					//$('#authy-countries').remove();
					//$('#countries-input-0').remove();
					//$('#country-code-0').remove();
					//$("#authy-countries").insertBefore("#phone_number");
					//$("#countries-input-0").insertBefore("#phone_number");
					//$("#country-code-0").insertBefore("#phone_number");
					
					$("#countryDiv").append($("#authy-countries")[0]);
					$("#countryDiv").append($("#countries-input-0")[0]);
					$("#countryDiv").append($("#country-code-0")[0]);
					
//					$("#authy-countries").show();
					$('.dimm').hide();
					$('#smsPopUp').hide();
			},
			error: function (e) {
				var errorData = JSON.parse(JSON.stringify(e));
				if(errorData.responseText == "success"){
					//$('#authy-countries').remove();
					//$('#countries-input-0').remove();
					//$('#country-code-0').remove();
					//$("#authy-countries").insertBefore("#phone_number");
					//$("#countries-input-0").insertBefore("#phone_number");
					//$("#country-code-0").insertBefore("#phone_number");
					
					$("#countryDiv").append($("#authy-countries")[0]);
					$("#countryDiv").append($("#countries-input-0")[0]);
					$("#countryDiv").append($("#country-code-0")[0]);
//					$("#authy-countries").show();
					$('.dimm').hide();
					$('#smsPopUp').hide();
				}else{
					alert(eval('msgP.'+langCode+'.incorrectCode'));
				}

			}
		});
	});

	//OTP 인증 POPUP Action
	$("#OtpSubmitPopUp").on("click",function (event) {
		
		//csrf 설정
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");	
		
		var search = {}
	    search["otpCode"] = $("#otpCodePopUp").val();
	    search["email"] = $("#email").val();

		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: "/Mypage/otpVerified",
			data: JSON.stringify(search),
			dataType: 'json',
			cache: false,
			timeout: 600000,
			beforeSend: function( xhr ) {
			    xhr.setRequestHeader(header, token);
			},
			success: function (data) {
				$('#success_auth').show();
				$('#infoAuth').show();  
			},
			error: function (e) {
				var errorData = JSON.parse(JSON.stringify(e));
				if(errorData.responseText == "success"){
					$('.dimm').hide();
					$('#otpPopUp').hide();
				}else{
					//alert("OTP 번호가 틀립니다.");
					alert(eval('msgP.'+langCode+'.incorrectOtp'));
				}

			}
		});
	
	});

});