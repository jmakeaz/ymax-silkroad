$(document).ready(function() {
	var imgSrc = $('html').find('img').eq(0).attr('src');
	var langCode = imgSrc.substring(imgSrc.indexOf('.net')+5, imgSrc.indexOf('/images'));
	var langArray = "en|ko|ru|jp|cn";
	if(langCode == null || langArray.indexOf(langCode) < 0){
		langCode = 'ko';
	}
	$("#join1submit").on("click",function (event) {    
		if($('input:checkbox[id="check01"]').is(":checked") == false){
			//alert('이용약관에 동의하여 주십시요.');
			alert(eval('msgP.'+langCode+'.termsAgree'));
			return false;
		}
		if($('input:checkbox[id="check02"]').is(":checked") == false){
			//alert('개인정보 수집 및 이용에 동의하여 주십시요.');
			alert(eval('msgP.'+langCode+'.privacyAgree'));
			return false;
		}
		$(location).attr('href', '/User/regiForm');
    });
	
	$("#join1back").on("click",function (event) {    
		event.preventDefault();
		history.back(1);
    });
});