
$(document).ready(function() {

	var imgSrc = $('html').find('img').eq(0).attr('src');
	var langCode = imgSrc.substring(imgSrc.indexOf('.net')+5, imgSrc.indexOf('/images'));
	var langArray = "en|ko|ru|jp|cn";
	if(langCode == null || langArray.indexOf(langCode) < 0){
		langCode = 'ko';
	}
	//Sms 인증번호 액션
	$("#sendMail").on("click",function (event) {
		$(".loading-group").show();
		//csrf 설정
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");	
		
		var search = {}
	    search["authCode"] = $("#mailAuthCode").val();
	    search["qrImgSrc"] = $("#qrCodeUrl").val();
	    search["email"] = $("#email").val();

		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: "/Mypage/otpFormMail",
			data: JSON.stringify(search),
			dataType: 'json',
			cache: false,
			timeout: 600000,
			beforeSend: function( xhr ) {
			    xhr.setRequestHeader(header, token);
			},
			success: function (data) {
				$('#success_auth').show();
				$('#infoAuth').show(); 
				$(".loading-group").hide();
			},
			error: function (e) {
				$(".loading-group").hide();
				//alert("ERROR !!!! : " + JSON.stringify(e));
				
				var errorData = JSON.parse(JSON.stringify(e));
				if(errorData.responseText == "success"){

					//alert("메일 전송에 성공했습니다.");
					alert(eval('msgP.'+langCode+'.sendMail'));
				}else{
					//alert("메일 전송에 실패했습니다.");
					alert(eval('msgP.'+langCode+'.failSendMail'));
				}

			}
		});
	
	});

	$("#popUpClose").on("click",function (event) {
		$('.dimm').hide();
		$('.ui_layer_popup').hide();
		$(location).attr('href', '/Mypage/info');
	});

	//Sms 인증번호 액션
	$("#otp1Submit").on("click",function (event) {

		if($('#password').val().length == 0){
			//alert('비밀 번호를 입력해 주십시요.');
			alert(eval('msgP.'+langCode+'.insPwd'));
            $('#password').val('');
            $('#password').focus();
			return false;
		}
		
		//인증번호 체크
		if($('#authCode').val().length == 0){
			//alert('인증암호키를 입력해 주십시요.');
			alert(eval('msgP.'+langCode+'.insAuthPwd'));
            $('#authCode').val('');
            $('#authCode').focus();
			return false;
		}
		
		//인증번호 체크
		if($('#otpCode').val().length == 0){
			//alert('OTP 코드를 입력해 주십시요.');
			alert(eval('msgP.'+langCode+'.insOtp'));
            $('#otpCode').val('');
            $('#otpCode').focus();
			return false;
		}

		//csrf 설정
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");	
		
		var search = {}
	    search["password"] = $("#password").val();
	    search["authCode"] = $("#authCode").val();
	    search["otpCode"] = $("#otpCode").val();
	    search["email"] = $("#email").val();

//		alert(JSON.stringify(search));
		$(".loading-group").show();
		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: "/Mypage/otpFormAction",
			data: JSON.stringify(search),
			dataType: 'json',
			cache: false,
			timeout: 600000,
			beforeSend: function( xhr ) {
			    xhr.setRequestHeader(header, token);
			},
			success: function (data) {
				$(".loading-group").hide();
			},
			error: function (e) {
				$(".loading-group").hide();
				var data = JSON.parse(JSON.stringify(e));
				if(data.responseText == "success"){
//					alert('success?');
					uiPopup.init();
				}else if(data.responseText == "nouser"){ //해당하는 유저가 없슴
					//alert("해당하는 유저가 없습니다.");
					alert(eval('msgP.'+langCode+'.noUser'));
				}else if(data.responseText == "otp"){ //otp code 불일치
					//alert("인증키와 OTP CODE가 불일치 합니다.");
					alert(eval('msgP.'+langCode+'.incorrect'));
				}else if(data.responseText == "password"){ //비밀번호 틀림
					//alert("비밀번호가 틀립니다.");
					alert(eval('msgP.'+langCode+'.incorrectPwd'));
				}
			}
		});
	
	});

});