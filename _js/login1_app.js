
//var recaptchaChecked = false;

$(document).ready(function() {
	var imgSrc = $('html').find('img').eq(0).attr('src');
	var langCode = imgSrc.substring(imgSrc.indexOf('.net')+5, imgSrc.indexOf('/images'));
	var langArray = "en|ko|ru|jp|cn";
	if(langCode == null || langArray.indexOf(langCode) < 0){
		langCode = 'ko';
	}
	
	$("#login1submit2").on("click",function (event) { //비밀번호 찾기
		$(location).attr('href', '/User/pwEmailForm');
	});

	$("#login1submit3").on("click",function (event) { //회원가입
		$(location).attr('href', '/User/terms');
	});
	$("#username").keyup(function() {
		//한글 안됨
		var regex = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/g;
		var email = $('#username').val();
		if (regex.test(email) === true) {
			$('#username').val('');
            $('#username').focus();
			return false;
		}else{
		}
	});
	//로그인 Submit 액션
	$("#login1submit1").on("click",function (event) {    
		//Email 체크
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		var email = $('#username').val();
		if (regex.test(email) === false) {
            $('#username').val('');
            $('#username').focus();
			return false;
        }
		/*
		//구글 체크?
		if(!recaptchaChecked){
			alert(eval('msgP.'+langCode+'.chkRecaptcha'));
			return false;
		}
		*/

		$('#myform').attr('action','/User/login').submit();

    });


});


function recaptchaCallback() {
	recaptchaChecked = true;
};
