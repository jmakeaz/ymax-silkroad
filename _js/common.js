// 구축시 진행된 스크립트 나열함.
// 필요없는 부분은 모두 지우고 정리 할 것.
// if(location.search.indexOf('ll=true') != -1){
// 	document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':8888/livereload.js"></' + 'script>');
// 	// HTML 구축시만 필요 개발 진행시 이 부분은 삭제
// }

// Avoid `console` errors in browsers that lack a console.
(function() {
	var method;
	var noop = function () {};
	var methods = [
		'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
		'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
		'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
		'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
	];
	var length = methods.length;
	var console = (window.console = window.console || {});

	while (length--) {
		method = methods[length];

		// Only stub undefined methods.
		if (!console[method]) {
			console[method] = noop;
		}
	}
}());

// cookie plugin
!function(e){var n=!1;if("function"==typeof define&&define.amd&&(define(e),n=!0),"object"==typeof exports&&(module.exports=e(),n=!0),!n){var t=window.Cookies,o=window.Cookies=e();o.noConflict=function(){return window.Cookies=t,o}}}(function(){function e(){for(var e=0,n={};e<arguments.length;e++){var t=arguments[e];for(var o in t)n[o]=t[o]}return n}function n(t){function o(n,r,i){var c;if("undefined"!=typeof document){if(arguments.length>1){if(i=e({path:"/"},o.defaults,i),"number"==typeof i.expires){var a=new Date;a.setMilliseconds(a.getMilliseconds()+864e5*i.expires),i.expires=a}try{c=JSON.stringify(r),/^[\{\[]/.test(c)&&(r=c)}catch(s){}return r=t.write?t.write(r,n):encodeURIComponent(String(r)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g,decodeURIComponent),n=encodeURIComponent(String(n)),n=n.replace(/%(23|24|26|2B|5E|60|7C)/g,decodeURIComponent),n=n.replace(/[\(\)]/g,escape),document.cookie=[n,"=",r,i.expires?"; expires="+i.expires.toUTCString():"",i.path?"; path="+i.path:"",i.domain?"; domain="+i.domain:"",i.secure?"; secure":""].join("")}n||(c={});for(var p=document.cookie?document.cookie.split("; "):[],u=/(%[0-9A-Z]{2})+/g,d=0;d<p.length;d++){var f=p[d].split("="),l=f.slice(1).join("=");'"'===l.charAt(0)&&(l=l.slice(1,-1));try{var m=f[0].replace(u,decodeURIComponent);if(l=t.read?t.read(l,m):t(l,m)||l.replace(u,decodeURIComponent),this.json)try{l=JSON.parse(l)}catch(s){}if(n===m){c=l;break}n||(c[m]=l)}catch(s){}}return c}}return o.set=o,o.get=function(e){return o.call(o,e)},o.getJSON=function(){return o.apply({json:!0},[].slice.call(arguments))},o.defaults={},o.remove=function(n,t){o(n,"",e(t,{expires:-1}))},o.withConverter=n,o}return n(function(){})});

//jQuery.noConflict();
!function($) {
	'use strict';

	$(function(){
		if($('.eux_prototype').length === !0){
			viewPortCheck.init();
		}
		viewPortCheck.init();
		initUI.setup();
		btnTop.init();
		mainFst.init();
		tokenBg.init();

		header.init(); // 개발언어로 변경시 이 부분 삭제 해야 합니다. (개발언어로 인클루드 필요.)
		footer.init();
		//퍼블리싱 전용 (주의!!! 개발 완료시 모두 삭제)/////////////////////////////
		// if(location.port == '8888' || location.hostname.indexOf('uxdev.etribe.co.kr') != -1|| location.hostname.indexOf('13.231.139.18') != -1){
		// 	header.init(); // 개발언어로 변경시 이 부분 삭제 해야 합니다. (개발언어로 인클루드 필요.)
		// 	footer.init(); // 개발언어로 변경시 이 부분 삭제 해야 합니다. (개발언어로 인클루드 필요.)


		// 	// mac os 일 경우 html 태그에 mac_os 클래스 붙임
		// 	if (navigator.userAgent.indexOf('Mac OS X') != -1) {
		// 		$("html").addClass("mac_os");
		// 	}
		// }
		/////////////////////////////////////////////////////////////////////////////
	});

	var isIE8 = $('html').hasClass('ie8');
	var isIE = (function detectIE() {
		var ua = window.navigator.userAgent;

		// Test values; Uncomment to check result …

		// IE 10
		// ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

		// IE 11
		// ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

		// Edge 12 (Spartan)
		// ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

		// Edge 13
		// ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

		var msie = ua.indexOf('MSIE ');
		if (msie > 0) {
			// IE 10 or older => return version number
			return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
		}

		var trident = ua.indexOf('Trident/');
		if (trident > 0) {
			// IE 11 => return version number
			var rv = ua.indexOf('rv:');
			return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
		}

		var edge = ua.indexOf('Edge/');
		if (edge > 0) {
			// Edge (IE 12+) => return version number
			return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
		}

		// other browser
		return false;
	})();
	isIE8 = isIE;
	var isIOS = (/iPad|iPhone|iPod/.test(navigator.userAgent || navigator.vendor || window.opera) && !window.MSStream);

	var initUI = (function(){
		var isLoaded;

		function setup(){
			if(isLoaded){
				return;
			}
			isLoaded = true;

			registUI('.swiper-container', uiSwiperComm, false);
			registUI('.ui_main_team', uiMainFt, false);
			registUI('.main', parallax, false);
			registUI('.ui_accordion', uiAccordion, false);
			registUI('.ui_select_date', uiSelectDate, false);
			registUI('html', comScroll, false);
			registUI('.quick_menu', uiQuickMenu, false);

			// 공통 적용
			textPlaceHolderInit(); // ie7,8 가능한 placeholder

		}

		function registUI(el, fn, saveData){
			if(saveData === undefined){
				saveData = true;
			}

			var _inst;

			$(el).each(function(idx, obj){
				_inst = new fn();
				_inst.init(obj, el);
				if(saveData){
					$(el).data('_inst', _inst);
				}
			});
		}

		return {
			setup: setup
		};
	})();

	// placeholder
	window.textPlaceHolderInit = function(_selector){

		var havePlaceholder = false;
		var input = document.createElement('input');
		havePlaceholder = ('placeholder' in input);
		var selectEl;

		if(_selector && _selector.length > 0){
			selectEl = _selector.find('input[type=text], textarea, input[type=password], textarea');
		}else{
			selectEl = $('input[type=text], textarea, input[type=password], textarea');
		}

		if(!havePlaceholder){
			selectEl.each(function(idx, obj){
				var _this = $(this);
				var placeholderAttr = 'placeholder';

				var placeholderText = _this.attr(placeholderAttr);

				/*
				if(_this.val() == ''){
					_this.val(placeholderText);
				}
				*/
				if(_this.prev('.placeholder_guidetext').length <= 0){
					_this.wrap('<span class="placeholder_wrap" style="display:inline-block;position:relative;"></span>');

					if(_this.hasClass('input_b')){
						_this.before('<span class="placeholder_guidetext bold"></span>');
					}else{
						_this.before('<span class="placeholder_guidetext"></span>');
					}

					var prevGuideText = _this.prev('.placeholder_guidetext');
					prevGuideText.text(placeholderText);
					prevGuideText.hide();
					if(_this.val() == ''){
						prevGuideText.show();
					}

					if(_this.css('text-align') == 'right'){
						prevGuideText.css({left: 'auto', right: 13});
					}

					prevGuideText.addClass('placeholder_text');

					_this.bind('mousedown focusin', function(e){
						if(!$(this).attr('disabled') || !$(this).attr('readonly')){
							prevGuideText.hide();
						}
					}).bind('focusout', function(e){
						if($(this).val() == ''){
							prevGuideText.show();
						}
					});

					prevGuideText.bind('mousedown', function(e){
						if(!$(this).next(input).attr('disabled') || !$(this).next(input).attr('readonly')){
							$(this).hide();
						}
						setTimeout(function(){
							_this.focus();
						}, 100);
					});
				}
			});
		}
	};

	// header include
	var header = (function(){
		var el;

		function init(){
			el = $('#header');

			if(el.length && el.children().length <= 0){
				window.header = $.get('/inc/layout/header.html');
				window.header.done(function(data){
					el.html(data);

					setTimeout(function(){
						complete();
					}, 0);
				});
			}else{
				complete();
			}
		}

		function complete(){
			headerFt.init();
		}

		return {init: init};
	})();

	// footer include
	var footer = (function(){
		var el;

		function init(){
			el = $('#footer');

			if(el.length && el.children().length <= 0){
				window.footer = $.get('/inc/layout/footer.html');
				window.footer.done(function(data){
					el.html(data);

					setTimeout(function(){
						complete();
					}, 0);
				});
			}else{
				complete();
			}
		}

		function complete(){
			/*
			ex)

			familysite등의 footer에 속한 스크립트는 footer안에서 서술
			*/
			btnTop.init();
		}

		return {init: init};
	})();

	var viewPortCheck = (function(){
		var documentEl, prevClass, nowClass, deviceClass;
		var mobileSize, pcSize, tabletSize;
		var windowWidth;

		function init(){
			documentEl = $('#wrap');
			deviceClass = ['mobile', 'pc'];

			mobileSize = 767;
			pcSize = 768;

			bindEvents();
		}

		function bindEvents(){
			resizeEvent();
		}

		function resizeEvent(){
			$(window).on('load resize', function(){
				viewPort();
			});
		}

		function viewPort(){
			windowWidth = window.innerWidth || $(window).width();
			setTimeout(function(){
				prevClass = documentEl.attr('class');
			});
			if(windowWidth <= mobileSize){
				documentEl.attr('class', deviceClass[0]);
			} else if(windowWidth >= pcSize){
				documentEl.attr('class', deviceClass[1]);
			}

			nowClass = documentEl.attr('class');
			if (prevClass != nowClass && typeof prevClass !== 'undefined'){
	        	var param = {
		            prev : prevClass
		            ,now : nowClass
	      		}
	      		$(document).trigger('viewPort.changed', param);
    		}
		}

		return{
			init:init
			, viewPort:viewPort
		}
	})();

	var parallax = function(){
		var el, sceneEl, indicatorEl;
		var sceneTop, sceneBottom, startPosi, stopPosi, scrollTop, scrollBottom, windowWidth, windowHeight, eventValue, mainMtTimer;
		var phoneEl,defaultEl,btn_prev,btn_next,listEl01,listEl02,listEl03,listEl04, defaultTxt;

		function init(_el){
			el = $(_el);
			sceneEl = el.find('.scene');
			indicatorEl = $('.indicator > ul > li');

			sceneTop =[];
			sceneBottom = [];
			startPosi = [];
			stopPosi = [];
			windowWidth = window.innerWidth || $(window).width()
			windowHeight = window.innerHeight || $(window).height();
			scrollTop = $(window).scrollTop();
			eventValue = 600;

			bindEvents();
		}

		function bindEvents(){
			// scene03SetMotion();
			setStyle();
			sceneChk();
			buttonAnimation();
			events();

			$(document).on('ready viewPort.changed', function(e, param){
				console.log(param)
			});
		}

		function setStyle(){
			$('.scene01 .bg01, .scene01 .bg02').css('height',$('.main .scene01').height()+110);

			if(windowHeight > scrollTop){
				// TweenMax.set($('.main #header'),{autoAlpha : 0 })
				// TweenMax.to($('.main #header'), 1 , {autoAlpha : 1 })
			} else {
				$('.main #header').removeAttr('style');
			}
			// setTimeout(function(){
			// 	$('.group').addClass('on');
			// 	TweenMax.set($('.scene01 .cut02'),{autoAlpha : 0})
			// 	TweenMax.to($('.scene01 .cut02'),.2,{autoAlpha : 1})
			// },mainMtTimer);

			//main scene03 set style
			phoneEl = $('.service_swiper');
			defaultEl = $('.service_swiper .default');
			defaultTxt = $('.scene04 .sub_swiper .default_txt');
			btn_prev = $('.service_swiper .btn_prev');
			btn_next = $('.service_swiper .btn_next');
			listEl01 = $('.sub_swiper').find('>ul>li:eq(0)');
			listEl02 = $('.sub_swiper').find('>ul>li:eq(1)');
			listEl03 = $('.sub_swiper').find('>ul>li:eq(2)');
			listEl04 = $('.sub_swiper').find('>ul>li:eq(3)');

			TweenMax.set(phoneEl,{y : 250 , autoAlpha : 0})
			TweenMax.set(defaultEl,{x : 0 , autoAlpha : 1})
			TweenMax.set(btn_prev,{autoAlpha : 0})
			TweenMax.set(btn_next,{autoAlpha : 0})
			TweenMax.set(listEl01,{x : 150 , autoAlpha : 0})
			TweenMax.set(listEl02,{x : 210 , autoAlpha : 0})
			TweenMax.set(listEl03,{x : -210 , autoAlpha : 0})
			TweenMax.set(listEl04,{x : -150 , autoAlpha : 0})

			TweenMax.set($('.document .tit01 span,.scene02 .tit01 span,.scene03 .tit01 span,.scene04 .tit02,.scene05>.tit02,.scene06>.tit02 span,.scene07>.tit02 span') ,{autoAlpha:0, y : -50});
			TweenMax.set($('.scene05 .readmap_group .tit01,.scene05 .readmap_group .tit03,.scene05 .readmap_group .tit05') ,{autoAlpha:0, x : 100});
			TweenMax.set($('.scene05 .readmap_group .tit02,.scene05 .readmap_group .tit04') ,{autoAlpha:0, x : -100});
			TweenMax.set($('.scene05 .list01,.scene05 .list03,.scene05 .list05') ,{autoAlpha:0, x : -100});
			TweenMax.set($('.scene05 .list02,.scene05 .list04') ,{autoAlpha:0, x : 100});
		}

		function events(){
			$(window).on('load scroll', function(e){
				scrollTop = $(this).scrollTop();
				scrollBottom = scrollTop >= windowHeight ? scrollTop - windowHeight : windowHeight;
				scrollChk(scrollTop);
				gnbActive(scrollTop);
				scrollSceneMotion(scrollTop);
			});
		}

		function sceneChk(){
			sceneEl.each(function(index, value){
				sceneTop[index] = Math.ceil($(this).offset().top);
				sceneBottom[index] = Math.ceil($(this).height() + sceneTop[index]-1);
				startPosi[index] = $(this).offset().top - eventValue;
				stopPosi[index] = ($(this).height() + sceneTop[index]) - eventValue;
			});
		}

		function scrollChk(scrollTop){
			sceneChk();
			if((sceneTop[0]-110) <= scrollTop && sceneBottom[0] >= scrollTop){
				indicatorEl.find('>a').removeClass('on');
				indicatorEl.eq(0).find('>a').addClass('on');
			} else if(sceneTop[1] <= scrollTop && sceneBottom[1] >= scrollTop){
				indicatorEl.find('>a').removeClass('on');
				indicatorEl.eq(1).find('>a').addClass('on');
			} else if(sceneTop[2] <= scrollTop && sceneBottom[2] >= scrollTop){
				indicatorEl.find('>a').removeClass('on');
				indicatorEl.eq(2).find('>a').addClass('on');
			} else if(sceneTop[3] <= scrollTop && sceneBottom[3] >= scrollTop){
				indicatorEl.find('>a').removeClass('on');
				indicatorEl.eq(3).find('>a').addClass('on');
			} else if(sceneTop[4] <= scrollTop && sceneBottom[4] >= scrollTop){
				indicatorEl.find('>a').removeClass('on');
				indicatorEl.eq(4).find('>a').addClass('on');
			} else if(sceneTop[5] <= scrollTop && sceneBottom[5] >= scrollTop){
				indicatorEl.find('>a').removeClass('on');
				indicatorEl.eq(5).find('>a').addClass('on');
			} else if(sceneTop[6] <= scrollTop && sceneBottom[6] >= scrollTop){
				indicatorEl.find('>a').removeClass('on');
				indicatorEl.eq(6).find('>a').addClass('on');
			} else if(sceneTop[7] <= scrollTop && sceneBottom[7] >= scrollTop){
				indicatorEl.find('>a').removeClass('on');
				indicatorEl.eq(7).find('>a').addClass('on');
			} else if(sceneTop[8] <= scrollTop){
				indicatorEl.find('>a').removeClass('on');
				indicatorEl.eq(8).find('>a').addClass('on');
			}
		}

		function scrollSceneMotion(scrollTop){
			if(startPosi[1] <= scrollTop && stopPosi[1] >= scrollTop){
				TweenMax.to($('.document .tit01 span'), .8 ,{delay: .5, autoAlpha:1, y : 0});
			} else if(startPosi[2] <= scrollTop && stopPosi[2] >= scrollTop){
				TweenMax.to($('.scene02 .tit01 span'), .8 ,{delay: .5, autoAlpha:1, y : 0});
			} else if(startPosi[3] <= scrollTop && stopPosi[3] >= scrollTop){
				TweenMax.to($('.scene03 .tit01 span'), .8 ,{delay: .5, autoAlpha:1, y : 0});
			} else if(startPosi[4] <= scrollTop && stopPosi[4] >= scrollTop){
				TweenMax.to($('.scene04 .tit02'), .8 ,{delay: .5, autoAlpha:1, y : 0});
				setTimeout(function(){
					scene03Motion();
				},1500);
			} else if(startPosi[5] <= scrollTop && stopPosi[5] >= scrollTop){
				TweenMax.to($('.scene05>.tit02'), .8 ,{delay: .5, autoAlpha:1, y : 0});
				TweenMax.to($('.scene05 .readmap_group .tit01'), .8 ,{delay: 2, autoAlpha:1, x : 0});
				TweenMax.to($('.scene05 .readmap_group .list01'), .8 ,{delay: 2.5, autoAlpha:1, x : 0});
				TweenMax.to($('.scene05 .readmap_group .tit02'), .8 ,{delay: 4, autoAlpha:1, x : 0});
				TweenMax.to($('.scene05 .readmap_group .list02'), .8 ,{delay: 4.5, autoAlpha:1, x : 0});
				TweenMax.to($('.scene05 .readmap_group .tit03'), .8 ,{delay: 6, autoAlpha:1, x : 0});
				TweenMax.to($('.scene05 .readmap_group .list03'), .8 ,{delay: 6.5, autoAlpha:1, x : 0});
				TweenMax.to($('.scene05 .readmap_group .tit04'), .8 ,{delay: 8, autoAlpha:1, x : 0});
				TweenMax.to($('.scene05 .readmap_group .list04'), .8 ,{delay: 8.5, autoAlpha:1, x : 0});
				TweenMax.to($('.scene05 .readmap_group .tit05'), .8 ,{delay: 10, autoAlpha:1, x : 0});
				TweenMax.to($('.scene05 .readmap_group .list05'), .8 ,{delay: 10.5, autoAlpha:1, x : 0});
			} else if(startPosi[6] <= scrollTop && stopPosi[6] >= scrollTop){
				TweenMax.to($('.scene06>.tit02 span'), .8 ,{delay: .5, autoAlpha:1, y : 0});
			} else if(startPosi[7] <= scrollTop && stopPosi[7] >= scrollTop){
				// TweenMax.to($('.scene07>.tit02 span'), .8 ,{delay: .5, autoAlpha:1, y : 0});
			} else if(startPosi[8] <= scrollTop && stopPosi[8] >= scrollTop){
				TweenMax.to($('.scene07>.tit02 span'), .8 ,{delay: .5, autoAlpha:1, y : 0});
			}
		}

		function buttonAnimation(){
			indicatorEl.find('>a').on('click', function(e){
				e.preventDefault();
				var _this = $(this);
				var idx = _this.closest('li').index();
				scrollTop = $(window).scrollTop();
				sceneChk();

				indicatorEl.find('>a').removeClass('on');
				indicatorEl.eq(idx).find('>a').addClass('on');
				if(idx === 0){
					$("html, body").animate({ scrollTop: 0}, 300);
				} else {
					$("html, body").animate({ scrollTop: sceneTop[idx]}, 300);
				}
				scrollChk(scrollTop);
			});
		}

		function gnbActive(scrollTop){
			var gnbEl = $('#header .gnb .gnb_wrap>ul>li');

			if((sceneTop[0]-110) <= scrollTop && sceneBottom[4] >= scrollTop){
				gnbEl.removeClass('on');
				gnbEl.eq(0).addClass('on');
				// 20180419 백서 별도 처리
				if(sceneTop[1]<= scrollTop && sceneBottom[1] >= scrollTop){
					gnbEl.removeClass('on');
					gnbEl.eq(5).addClass('on');
				}
			} else if((sceneTop[5]) <= scrollTop && sceneBottom[5] >= scrollTop){
				gnbEl.removeClass('on');
				gnbEl.eq(1).addClass('on');
			} else if((sceneTop[6]) <= scrollTop && sceneBottom[6] >= scrollTop){
				gnbEl.removeClass('on');
				gnbEl.eq(2).addClass('on');
			} else if((sceneTop[8]-1) <= scrollTop && sceneBottom[8] >= scrollTop){
				gnbEl.removeClass('on');
				gnbEl.eq(3).addClass('on');
			} else {
				gnbEl.removeClass('on');
			}
		}

		function scene03SetMotion(){
			TweenMax.set($('.circle'),{autoAlpha:0})
			TweenMax.to($('.circle.t01'), 2.8 ,{yoyo:true, repeat : -1 ,autoAlpha:.2});
			TweenMax.to($('.circle.t02'), 3 ,{delay: 1.2 , yoyo:true, repeat : -1 ,autoAlpha:.2});
			TweenMax.to($('.circle.t03'), 2.5 ,{delay: 1.5 , yoyo:true, repeat : -1 ,autoAlpha:.2});
		}

		function scene03Motion(){
			TweenMax.to(phoneEl, .8 , {y : 0 , autoAlpha : 1});
			TweenMax.to(defaultEl, .8 , {delay:5 , x : -350, autoAlpha : 0, ease:Power4.easeNone});
			TweenMax.to(defaultTxt, .8 , {delay:4.9 , autoAlpha :0, onComplete: function(){
				 defaultTxt.css('display','none');
				$('.scene04 .sub_swiper .title_area').css('display','block');
			}});
			TweenMax.to(btn_prev, .8 , {delay:5 , autoAlpha : 1, ease:Power4.easeNone});
			TweenMax.to(btn_next, .8 , {delay:5 , autoAlpha : 1, ease:Power4.easeNone});
			TweenMax.to(listEl01, .8 , {delay:1.5 ,x : 0 , autoAlpha : 1});
			TweenMax.to(listEl02, .8 , {delay:1 ,x : 0 , autoAlpha : 1});
			TweenMax.to(listEl03, .8 , {delay:1 ,x : 0 , autoAlpha : 1});
			TweenMax.to(listEl04, .8 , {delay:1.5 ,x : 0 , autoAlpha : 1});
		}

		return{init:init}
	}

	var uiSwiperComm = function(){
		var el, _swiper;
		var _option, defaultOption;
		var prevEl, nextEl, slideWrap;
		var txtEl, idx, maxCount, minCount, txtScene, startCheck;

		function init(_el){
			el = $(_el);
			slideWrap = $('.sub_swiper').find("ul");
			txtEl = $('.scene04').find('.title_area > .txt');
			idx = 1;
			minCount = 0;
			maxCount = 3;
			startCheck = true;

			bindEvent();
		}

		function bindEvent(){
			swiperType();
		}

		function swiperType(){
			el.each(function(index, value){
				if($(this).data("service")){
					_option = {
						speed: 400,
					    touchAngle : 40,
					    spaceBetween: 0,
					    loop : true,
					    onSlideChangeStart: function(swiper){
					    	if(!$('html').hasClass('ie9')){
					    		eachBrowserType(swiper);
					    	} else {
					    		ieTypeFc(swiper);
					    	}
					    }
					}
					_swiper = new Swiper(el, _option);
				}
			});
		}

		function ieTypeFc(swiper){
			txtScene = swiper.activeIndex;
	    	swipingTxt(txtScene);

	    	$('.service_swiper .btn_next').off().on('click', function(e){
	    		e.preventDefault();

	    		if(idx < maxCount){
	    			idx++;
	    		} else if(idx === maxCount){
					idx = 0;
				}
	    		swipeTxt(idx);
	    		_swiper.slideTo(idx);
	    	}).hover(function(){
				TweenMax.to( $(this), 0.6, { x : 6, ease:Back.easeInOut});
			}, function(){
				TweenMax.to( $(this), 0.3, { x : 0, ease:Circ.easeInOut});
			});
	    	$('.service_swiper .btn_prev').off().on('click', function(e){
	    		e.preventDefault();

	    		if(startCheck){
	    			idx = 0;
	    		}
	    		startCheck = false;
	    		if(idx>0){
	    			idx--;
	    		} else if(idx === 0){
					idx = maxCount;
				}
	    		_swiper.slideTo(idx);
	    		swipeTxt(idx);
	    	}).hover(function(){
				TweenMax.to( $(this), 0.6, { x : -6, ease:Back.easeInOut});
			}, function(){
				TweenMax.to( $(this), 0.3, { x : 0, ease:Circ.easeInOut});
			});
		}

		function eachBrowserType(swiper){
			txtScene = swiper.activeIndex;
	    	swipingTxt(txtScene);

	    	$('.service_swiper .btn_next').off().on('click', function(e){
	    		e.preventDefault();

	    		swipeTxt(idx);
	    		_swiper.slideNext();
	    	}).hover(function(){
				TweenMax.to( $(this), 0.6, { x : 6, ease:Back.easeInOut});
			}, function(){
				TweenMax.to( $(this), 0.3, { x : 0, ease:Circ.easeInOut});
			});
	    	$('.service_swiper .btn_prev').off().on('click', function(e){
	    		e.preventDefault();

	    		swipeTxt(idx);
	    		_swiper.slidePrev();
	    	}).hover(function(){
				TweenMax.to( $(this), 0.6, { x : -6, ease:Back.easeInOut});
			}, function(){
				TweenMax.to( $(this), 0.3, { x : 0, ease:Circ.easeInOut});
			});
		}

		function swipeTxt(idx){
			txtEl.hide();
			txtEl.eq(idx).show();
		}

		function swipingTxt(activeIndex){
	    	if(activeIndex > 1 && activeIndex <5){
	    		txtEl.hide();
	    		txtEl.eq(activeIndex -1).show();
	    	} else if(activeIndex === 0){
	    		txtEl.hide();
				txtEl.eq(3).show();
	    	} else if(activeIndex === 1){
	    		txtEl.hide();
				txtEl.eq(0).show();
	    	} else if(activeIndex === 5){
	    		txtEl.hide();
	    		txtEl.eq(0).show();
	    	}
		}


		return{init:init}
	}

	var uiMainFt = function(){
		var teamEl, contEl;
		var windowWidth, windowHeight;

		function init(){
			teamEl = $('.ui_main_team');
			contEl = teamEl.find('.details');
			windowWidth = window.innerWidth || $(window).width();
			windowHeight = window.innerHeight || $(window).height();

			$('.details.on').find('.acco_txt').show();

			bindEvents();
		}

		function bindEvents(){
			teamFt();
			// mainVideo();
		}

		function teamFt(){
			$('.details').find('.btn_more').on('click', function(e){
				e.preventDefault();
				var _this = $(this);

				_this.closest('.details').addClass('on');
				_this.closest('.details').find('.acco_txt').slideDown();
			});
			$('.details').find('.btn_hide').on('click', function(e){
				e.preventDefault();
				var _this = $(this);

				_this.closest('.details').removeClass('on');
				_this.closest('.details').find('.acco_txt').slideUp();
			});
		}

		function mainVideo(){
			var videoEl = $('#vid'),
				btnEl = $('.vedio_wrap').find('.btn_play');
			videoEl.on('mouseenter', function(e){
				// TweenMax.to(btnEl, .5 , {autoAlpha:1});
			});
			btnEl.on('click', function(e){
				e.preventDefault();

				videoEl[0].play();
				TweenMax.to($(this), .5 , {autoAlpha:0});
			});
		}

		return{init:init}
	}

	var uiAccordion = function(){
		var el, btnEl, contEl;

		function init(_el){
			el = $(_el);
			btnEl = el.find('.btn_acco');
			contEl = el.find('.acc_cont');

			bindEvents();
		}

		function bindEvents(){
			clickEvent();
		}

		function clickEvent(){
			btnEl.on('click', function(e){
				e.preventDefault();
				var _this = $(this);

				contEl.slideUp();
				btnEl.removeClass('on');
				if(_this.siblings('.acc_cont').is(':hidden')){
					_this.addClass('on');
					_this.siblings('.acc_cont').slideDown();
				} else {
					_this.removeClass('on');
					_this.siblings('.acc_cont').slideUp();
				}
			});
		}

		return{init:init}
	}

	window.headerFt = (function(){
		var btnMnavi, gnbLayer, mLangLayer, langBtn;
		var el, idx, indiEl, _depth1;

		function init(){
			el = $('#header');
			btnMnavi = el.find('.btn_menu');
			gnbLayer = el.find('.gnb');
			_depth1 = el.find('nav>ul>li');
			indiEl = $('.indicator>ul>li');
			langBtn = el.find('.m_language > .btn_language');
			mLangLayer = el.find('.m_language > ul');

			bindEvent();
		}

		function bindEvent(){
			mobileMenu();
			headFt();
			clickEvent();
			mLanguage();
		}

		function mobileMenu(){
			btnMnavi.on('click', function(e){
				e.preventDefault();

				$(this).toggleClass('active');
				if(!gnbLayer.hasClass('on')){
					gnbLayer.addClass('on');
					TweenMax.set(gnbLayer,{autoAlpha : 0 , y : -100})
					TweenMax.set(gnbLayer,{css:{display:"block"}});
					TweenMax.to(gnbLayer, .5 ,{autoAlpha : 1 , y : 0});
				} else {
					gnbLayer.removeClass('on');
					TweenMax.to(gnbLayer, .5 ,{autoAlpha : 0 , y : -100 , onComplete:function(){
						TweenMax.to(gnbLayer,0,{css:{display:"none"}});
					}});
				}
			});
		}

		function mLanguage(){
			langBtn.on('click', function(e){
				e.preventDefault();

				if(mLangLayer.is(':visible')){
					$(this).removeClass('on');
					mLangLayer.hide();
				} else {
					$(this).addClass('on');
					mLangLayer.show();
				}
			});
		}

		function headFt(){
			$('.language_group').on('click', '.btn_language', function(e){
				e.preventDefault();

				$(this).closest('.language_group').toggleClass('on');
			});
		}

		function clickEvent(){
			_depth1.find('a').on('click', function(e){
				idx = $(this).closest('li').index();

				indiEl.find('a').removeClass('on');
				if(idx === 0){
					indiEl.eq(idx).find('a').addClass('on');
				} else if(idx === 1){
					indiEl.eq(4).find('a').addClass('on');
				} else if(idx === 2){
					indiEl.eq(5).find('a').addClass('on');
				} else if(idx === 3 || idx === 4){
					indiEl.eq(6).find('a').addClass('on');
				}
			});
		}

		function activeGnb(){
			indiEl.find('a').removeClass('on');
			if(idx === 0){
				indiEl.eq(0).find('a').addClass('on');
			} else if(idx === 4){
				indiEl.eq(4).find('a').addClass('on');
			} else if(idx === 5){
				indiEl.eq(5).find('a').addClass('on');
			}
		}

		return{
			init:init,
			mobileMenu:mobileMenu,
			headFt:headFt
		}
	})();

	var btnTop = (function(){
		var windowWidth, footerHeight;
		function init(){
			windowWidth = window.innerWidth || $(window).width();
			footerHeight = $('#footer').height();

			$(window).off('load.top_load resize.top_resize scroll.top_scroll').on('load.top_load resize.top_resize scroll.top_scroll', function(e){
				windowWidth = window.innerWidth || $(window).width();
				var position = (windowWidth - 1200) / 2;

				topPosition(position);
			});
		}

		function topPosition(posi){
			if(windowWidth >= 1200){
				$('.fixed_top').css('right', posi);
			}
			if(($('#wrap').height()-footerHeight)-$(window).height() <= $(window).scrollTop()){
				$('.fixed_top').css('bottom', footerHeight);
			} else {
				$('.fixed_top').css('bottom', 46);
			}
		}

		return{init:init}
	})();

	window.tokenGraph = (function(){
		var el, gaugeEl, txtEl;
		var minValue, maxValue, txtSize;

		function init(min, max){
			el = $('.ui_gauge');
			gaugeEl = el.find('.gauge');
			txtEl = el.find('.min_value');

			txtSize = txtEl.width() / 2;
			minValue = min;
			maxValue = max;

			bindEvents();
		}

		function bindEvents(){
			graphAnimation();
		}

		function setStyle(){
			el = $('.ui_gauge');
			txtEl = el.find('.min_value');
			txtSize = txtEl.width() / 2;

			txtEl.css('margin-left',-txtSize);
		}

		function graphAnimation(){
			txtEl.css('margin-left',-txtSize);
			TweenMax.set(gaugeEl, {width : minValue + '%'})
			TweenMax.to(gaugeEl , 1 ,{width : maxValue + '%' , ease:Power4.easeOut});
		}

		return{init:init,setStyle:setStyle}
	})();

	var uiSelectDate = function(){
		var el, btnEl, idx;

		function init(_el){
			el = $(_el);
			btnEl = el.find('ul li');

			clickEvent();
		}

		function clickEvent(){
			btnEl.find('a').on('click', function(e){
				e.preventDefault();
				idx = $(this).closest('li').index();

				btnEl.find('a').removeClass('on');
				$(this).addClass('on');
			});
		}

		return{init:init}
	}

	window.uiPopup = (function(){
		var el;
		var layerHeight, windowHeight, windowWidth, elWidth, elHeight;

		function init(){
			el = $('.ui_layer_popup');
			layerHeight = el.height() / 2;
			windowWidth = window.innerWidth || $(window).width();
			windowHeight = window.innerHeight || $(window).height();
			elWidth = el.width();
			elHeight = el.height();

			styleFt();
		}

		function styleFt(){
			$('.dimm').css('z-index','152').show();
			el.show();
			if(windowWidth != elWidth){
				el.css('margin-top',-layerHeight+55);
			} else if(elWidth < 768 && elHeight < windowHeight){
				// 모바일 & 팝업창이 디바이스 높이보다 작을때
				el.css({
					'top' : '50%',
					'margin-top' : -layerHeight
				});
			}
			if(windowHeight <= elHeight){
				// 모바일 & 팝업창 스크롤
				el.css({
					'overflow-y' : 'auto',
					'height' : '100%'
				});
			}
		}

		return{init:init}
	})();

	var comScroll = function(){
		var scrollTop, setTimer, pageCheck;

		function init(){
			pageCheck = $('body.main').length || $('body.pre_sale').length;

			bindEvents();
		}

		function bindEvents(){
			scrollEvent();
		}

		function scrollEvent(){
			$(window).on('scroll', function(){
				scrollTop = $(this).scrollTop();
				gnbFixed(scrollTop);
			});
		}

		function gnbFixed(scrollTop){
			if(scrollTop > 0){
				$('#header').addClass('fixed');
				clearTimeout(setTimer);
				$('#header').css('background','rgba(0,0,0,.7)');
				$('.fixed_top').show();
			} else {
				$('#header').removeClass('fixed');
				setTimer;
				$('.fixed_top').hide();
			}
			setTimer = setTimeout(function(){
				!$('#header').hasClass('fixed') && pageCheck === 1 ? $('#header').css('background','rgba(0,0,0,0)') : $('#header').css('background','rgba(0,0,0,1)');
			},300);
		}

		return{init:init}
	}

	var mainFst = (function(){
		var mainDvCheck, mainCheck, videoLayer, windowWidth, windowHeight, videoHeight;
		function init(){
			videoLayer = $('.main .video_group video');
			mainCheck = $('body.main').length;

			windowWidth = window.innerWidth || $(window).width();
			windowHeight = window.innerHeight || $(window).height();

			if(mainCheck === 0){
				return false;
			}

			$(window).off('load.mLoad resize.mResize').on('load.mLoad resize.mResize', function(e){
				mainDvCheck = windowWidth < 768 ? 'mobile' : 'pc';
				windowWidth = window.innerWidth || $(window).width();
				windowHeight = window.innerHeight || $(window).height();
				if(mainDvCheck === 'pc'){
					pcFst();
				} else {
					mobileFst();
				}
			});
		}

		function mobileFst(){
			$('.main .scene01').css('height',windowHeight-56);
			$('.main .scene01>.bg').css('height',windowHeight);
			$('.main .scene01 .text_group').css('padding-top',($(window).height()-244) / 2);
			$('.main .scene01 .bg_group  .group').css('margin-top',($(window).height()/2) -108);
		}

		function pcFst(){
			videoHeight = videoLayer.height()-1;
			$('.main .scene01 .text_group').removeAttr('style');
			$('.main .scene01').css('height', videoHeight-110);
			$('.main .video_group').css('height', videoHeight);
		}

		return{
			init : init
		}
	})();

	var uiQuickMenu = function(){
		var el, btnEl, btnEachEl;

		function init(_el){
			el = $(_el);
			btnEl = el.find('.btn_quick');
			btnEachEl = el.find('.menu_list a');

			bindEvents();
		}

		function bindEvents(){
			clickEvent();
		}

		function clickEvent(){
			btnEl.on('click', function(e){
				e.preventDefault();

				if(!$(this).closest('.menu_area').hasClass('on')){
					var btnHeight = btnEl.height() + 10;
					$('.quick_menu').css('z-index','153');
					$('.dimm').css('z-index','152').show();
					btnEachEl.each(function(index, value){
						var idx = index + 1;
						TweenMax.set(value,{y:0 , autoAlpha:0})
						TweenMax.to(value, .4, {y:-btnHeight * idx , autoAlpha:1 , ease:Power4.easeNone});
					});
				} else {
					TweenMax.to(btnEachEl, .4, {y:0 , autoAlpha:0 , ease:Power4.easeNone , onComplete:function(){
						$('.quick_menu').removeAttr('style');
						$('.dimm').removeAttr('style').hide();
					}});
				}
				$(this).closest('.menu_area').toggleClass('on');
			});
		}

		return{init:init}
	}

	window.browserCheck = (function(){
		var browser, opera, deviceCheck, androidChrome;
		function init(){
			browser = navigator.userAgent.toLowerCase();
			opera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
			deviceCheck = "win16|win32|win64|mac|macintel";
			androidChrome = navigator.userAgent.match(/iPhone|iPod|Android|Windows CE|BlackBerry|Symbian|Windows Phone|webOS|Opera Mini|Opera Mobi|POLARIS|IEMobile|lgtelecom|nokia|SonyEricsson/i) != null || navigator.userAgent.match(/LG|SAMSUNG|Samsung/) != null;

			if(browser.indexOf('chrome') === -1 || navigator.userAgent.indexOf('Edge/') > 0 || opera || deviceCheck.indexOf(navigator.platform.toLowerCase()) < 0 || androidChrome){
				$('.dimm').css('z-index','152').show();
				$('.ui_layer_popup.brower').css('z-index','155').show();
			}
		}

		return{init:init}
	})();

	var tokenBg = (function(){
		var el, bgEl, visualEl;
		var bgHeight, windowWidth;

		function init(){
			el = $('.main .token');
			bgEl = el.find('> .bg');
			visualEl = el.find('.visual_group');

			setStyle();
		}

		function setStyle(){
			$(window).off('load.tokenLoad resize.tokenResize').on('load.tokenLoad resize.tokenResize', function(){
				windowWidth = window.innerWidht || $(window).width();
				bgHeight = visualEl.height() + 96;

				if(windowWidth < 1024){
					bgEl.css('height',bgHeight);
				} else if (windowWidth > 768){
					bgEl.removeAttr('style');
				}
			});
		}

		return{init:init}
	})();

}(jQuery);

