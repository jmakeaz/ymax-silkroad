$(document).ready(function() {
	//회원가입 Submit 액션
	var imgSrc = $('html').find('img').eq(0).attr('src');
	var langCode = imgSrc.substring(imgSrc.indexOf('.net')+5, imgSrc.indexOf('/images'));
	var langArray = "en|ko|ru|jp|cn";
	if(langCode == null || langArray.indexOf(langCode) < 0){
		langCode = 'ko';
	}
	
	var oldhtml = $('#limit').html();
	var newhtml = oldhtml.replace("${limitTime}", $('#limitTime').val());
	$('#limit').html(newhtml);


	$("#join3submit").on("click",function (event) {    
		
		//csrf 설정
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");	
		
		var search = {}
	    search["email"] = $("#username").val();

		//alert(JSON.stringify(search));

		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: "/User/sendVerifiedMail",
			data: JSON.stringify(search),
			dataType: 'json',
			cache: false,
			timeout: 600000,
			beforeSend: function( xhr ) {
			    xhr.setRequestHeader(header, token);
			},
			success: function (data) {

			},
			error: function (e) {
				
				//alert("ERROR !!!! : " + JSON.stringify(e));
				
				var errorData = JSON.parse(JSON.stringify(e));
				//alert(errorData.responseText);
				if(errorData.responseText == "nouser"){
					$("#join3submit").hide();
					//$("#message").text("해당되는 회원정보가 없습니다.");
					$("#message").text(eval('msgP.'+langCode+'.noUser'));
				}else if(errorData.responseText == "max"){
					$("#join3submit").hide();
					//$("#message").text("3회이상 전송하였습니다.");
					$("#message").text(eval('msgP.'+langCode+'.limitResend'));
				}else{
					if(errorData.responseText == "0"){
						$("#join3submit").hide();
						//$("#message").text("3회이상 전송하였습니다.");
						$("#message").text(eval('msgP.'+langCode+'.limitResend'));
					}else{
						$(".color_t03").text(errorData.responseText);
					}
				}

			}
		});
	
    });
});