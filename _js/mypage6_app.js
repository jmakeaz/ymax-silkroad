
window.uiPopupOTP = (function(){
	var el;
	var layerHeight;

	function init(){
		el = $('#otpPopUp');
		layerHeight = el.height() / 2;

		styleFt();
	}

	function styleFt(){
		$('.dimm').show();
		el.show();
		el.css('margin-top',-layerHeight+55);
		//console.log(layerHeight)
	}

	return{init:init}
})();

window.uiPopupSms = (function(){
	var el;
	var layerHeight;

	function init(){
		el = $('#smsPopUp');
		layerHeight = el.height() / 2;
		styleFt();
	}

	function styleFt(){
		$('.dimm').show();
		el.show();
		el.css('margin-top',-layerHeight+55);
		//console.log(layerHeight);
	}

	return{init:init}
})();

$(document).ready(function() {
	var imgSrc = $('html').find('img').eq(0).attr('src');
	var langCode = imgSrc.substring(imgSrc.indexOf('.net')+5, imgSrc.indexOf('/images'));
	var langArray = ["en","ko","ru","jp","cn"];
	if(langCode == null || !langArray.includes(langCode)){
		langCode = 'en';
	}
	$("#newpassword").on("blur keyup",function() {
		var pswd = $(this).val();
		
		var pattern = /^.*(?=.{8,16})(?=.*[0-9])(?=.*[a-zA-Z]).*$/;

		if(!pattern.test(pswd)) {
			//alert("비밀번호는 8자리 이상 문자, 숫자, 특수문자로 구성하여야 합니다."); 
			$('#error_password2').show();
		}else { 
			$('#error_password2').hide();
		} 
	});
	$("#passwordConfirm").on("keyup blur",function() {
		var pswd = $("#newpassword").val();
		var pswdChk = $(this).val();
		
		if(pswd === pswdChk){
			$('#error_confirm_password').hide();
		}else{
		//	$('#passwordConfirm').focus();
			$('#error_confirm_password').show();
		}
		//if( pswd.length > 0)$('#error_passwordConfirm').hide();
	});
	$("#mypage6Submit").on("click",function (event) {
		
		//인증번호 체크
		if($('#password').val().length == 0){
            $('#error_password').show();
            $('#password').val('');
            $('#password').focus();
			return false;
		}

		var pswd = $('#newpassword').val();
	    
		var pattern = /^.*(?=.{8,16})(?=.*[0-9])(?=.*[a-zA-Z]).*$/;

		if(!pattern.test(pswd)) {
	    	alert(eval('msgP.'+langCode+'.passwdChk'));
	    	$('#newpassword').focus();
	    	return false; 
	    }
	    
		if($('#newpassword').val().length == 0){
            $('#error_new_password').show();
            $('#newpassword').val('');
            $('#newpassword').focus();
			return false;
		}

		if($('#passwordConfirm').val().length == 0){
            $('#error_new_password2').show();
            $('#passwordConfirm').val('');
            $('#passwordConfirm').focus();
			return false;
		}
		
		//비밀번호 비교체크
		var p = $('#newpassword').val();
		var pConfirm  = $('#passwordConfirm').val();
		if(p != pConfirm){
            $('#error_confirm_password').show();
            $('#passwordConfirm').val('');
            $('#passwordConfirm').focus();
			return false;
		}

		$('#myform').attr('action','/Mypage/pdChangeAction').submit();
	});



//여기서부터 POPUP 관련
	$('#smsReSendPopUp').hide();  
	$('#success_smsPopUp').hide();
	$('#infoAuthPopUp').hide(); 
	$('#error_smsPopUp').hide(); 

	if(authMode == '1')
		uiPopupSms.init();
	else if(authMode == '2')
		uiPopupOTP.init();

	//SMS 인증 POPUP Action
	$("#mypage5SmsSubmitPopUp").on("click",function (event) {
		$('#smsReSendPopUp').show();  
		
		//csrf 설정
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");	
		
		var search = {}
	    search["change"] = 0;
	    search["email"] = $("#email").val();
	    search["phoneNumber"] = $("#phone_numberPopUp").val();
	    search["countryCode"] = $("input[name=country_code]").val();

		//alert(JSON.stringify(search));

		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: "/SMS/sendSMSAuth",
			data: JSON.stringify(search),
			dataType: 'json',
			cache: false,
			timeout: 600000,
			beforeSend: function( xhr ) {
			    xhr.setRequestHeader(header, token);
			},
			success: function (data) {
					$('#success_smsPopUp').show();
					$('#infoAuthPopUp').show();  
			},
			error: function (e) {
				var errorData = JSON.parse(JSON.stringify(e));
				if(errorData.responseText == "success"){
					$('#success_smsPopUp').show();
					$('#infoAuthPopUp').show();  
				}else{
					$('#error_smsPopUp').show();
				}

			}
		});
	
	});

	$("#smsReSendPopUp").on("click",function(event){
		$("#mypage5SmsSubmitPopUp").click();
	});

	//SMS 인증 확인버튼
	$("#SmsSubmitPopUp").on("click",function(event){
		
		//csrf 설정
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");	
		
		var search = {}
	    search["authCode"] = $("#authCodePopUp").val();
	    search["email"] = $("#email").val();

		//alert(JSON.stringify(search));

		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: "/SMS/verifiedSMSAuth",
			data: JSON.stringify(search),
			dataType: 'json',
			cache: false,
			timeout: 600000,
			beforeSend: function( xhr ) {
			    xhr.setRequestHeader(header, token);
			},
			success: function (data) {
					$('.dimm').hide();
					$('#smsPopUp').hide();
			},
			error: function (e) {
				var errorData = JSON.parse(JSON.stringify(e));
				if(errorData.responseText == "success"){
					$('.dimm').hide();
					$('#smsPopUp').hide();
				}else{
					//alert("인증 번호가 틀립니다.");
					alert(eval('msgP.'+langCode+'.incorrectCode'));
				}

			}
		});
	});

	//OTP 인증 POPUP Action
	$("#OtpSubmitPopUp").on("click",function (event) {
		
		//csrf 설정
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");	
		
		var search = {}
	    search["otpCode"] = $("#otpCodePopUp").val();
	    search["email"] = $("#email").val();

		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: "/Mypage/otpVerified",
			data: JSON.stringify(search),
			dataType: 'json',
			cache: false,
			timeout: 600000,
			beforeSend: function( xhr ) {
			    xhr.setRequestHeader(header, token);
			},
			success: function (data) {
				$('#success_auth').show();
				$('#infoAuth').show();  
			},
			error: function (e) {
				var errorData = JSON.parse(JSON.stringify(e));
				if(errorData.responseText == "success"){
					$('.dimm').hide();
					$('#otpPopUp').hide();
				}else{
					//alert("OTP 번호가 틀립니다.");
					alert(eval('msgP.'+langCode+'.incorrectOtp'));
				}

			}
		});
	
	});

});