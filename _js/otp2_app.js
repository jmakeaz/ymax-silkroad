
$(document).ready(function() {
	
	var imgSrc = $('html').find('img').eq(0).attr('src');
	var langCode = imgSrc.substring(imgSrc.indexOf('.net')+5, imgSrc.indexOf('/images'));
	var langArray = "en|ko|ru|jp|cn";
	if(langCode == null || langArray.indexOf(langCode) < 0){
		langCode = 'ko';
	}
	$(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
     $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

	 $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
	//회원가입 Submit 액션
	$("#otp2Submit").on("click",function (event) {    

		//비밀번호 체크
		if($('#password').val().length == 0){
			//alert('비밀번호를 입력해 주십시요.');
			alert(eval('msgP.'+langCode+'.insPwd'));
            $('#password').val('');
            $('#password').focus();
			return false;
		}
		
		//인증번호 체크
		if($('#otpCode').val().length == 0){
			//alert('인증번호를 입력해 주십시요.');
			alert(eval('msgP.'+langCode+'.insAuthcode'));
            $('#otpCode').val('');
            $('#otpCode').focus();
			return false;
		}
		$('#myform').attr('action','/Mypage/otpDisabled').submit();

    });


});
