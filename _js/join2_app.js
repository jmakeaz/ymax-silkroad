
//var recaptchaChecked = false;

$(document).ready(function() {
	var imgSrc = $('html').find('img').eq(0).attr('src');
	var langCode = imgSrc.substring(imgSrc.indexOf('.net')+5, imgSrc.indexOf('/images'));
	var langArray = ["en","ko","ru","jp","cn"];
	if(langCode == null || !langArray.includes(langCode)){
		langCode = 'en';
	}

	$('#lv4').hide();
	$('#lv3').hide();
	$('#lv2').hide();
	$('#lv1').hide();
	$('#join2reSend').hide();
	$('#infoAuth').hide();

    //code here
	$("#username").blur(function() {
		var regex = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;
		var email = $('#username').val();
		if (regex.test(email) === false) {
	        $('#error_email2').show();
            $('#username').focus();
			return false;
	    }
	}).keyup(function() {
		//한글 안됨
		var regex = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/g;
		var email = $('#username').val();
		if (regex.test(email) === true) {
			$('#username').val('');
			$('#error_email2').show();
            $('#username').focus();
			return false;
		}else{
			$('#error_email2').hide();
		}
	});
	
	$(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
     $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

	 $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });


	$("#password").on("blur keyup",function() {
		var pswd = $(this).val();
		
		var pattern = /^.*(?=.{8,16})(?=.*[0-9])(?=.*[a-zA-Z]).*$/;

		if(!pattern.test(pswd)) {
			$('#lv1').hide();
			$('#lv4').show();
			$('#error_password').show();
			return false; 
		}else { 
			$('#lv4').hide();
			$('#error_password').hide();
			$('#lv1').show();
			return true; 
		} 
		
	});

	
	$("#passwordConfirm").on("keyup blur",function() {
		var pswd = $("#password").val();
		var pswdChk = $(this).val();
		
		if(pswd === pswdChk){
			$('#error_passwordConfirm').hide();
		}else{
		//	$('#passwordConfirm').focus();
			$('#error_passwordConfirm').show();
		}
	});

	
	$("#join2login").on("click",function (event) {
		$(location).attr('href', '/User/login');
	});

	//Sms 인증번호 액션
	$("#join2SmsSubmit").on("click",function (event) {
		$('#join2reSend').show();  
		
		//csrf 설정
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");	
		
		var search = {}
	    search["email"] = $("#username").val();
	    search["phoneNumber"] = $("#phone_number").val();
	    search["countryCode"] = $("input[name=country_code]").val();

		//비밀번호 체크
		var p = $('#password').val();
		var pConfirm  = $('#passwordConfirm').val();
		if(p != pConfirm){
			$('#error_passwordConfirm').show();
            $('#passwordConfirm').val('');
            $('#passwordConfirm').focus();
			return false;
		}
		
		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: "/SMS/sendSMS",
			data: JSON.stringify(search),
			dataType: 'json',
			cache: false,
			timeout: 600000,
			beforeSend: function( xhr ) {
			    xhr.setRequestHeader(header, token);
			},
			success: function (data) {
				$('#success_auth').show();
				$('#infoAuth').show();  
			},
			error: function (e) {
				
				var errorData = JSON.parse(JSON.stringify(e));
				if(errorData.responseText == "success"){
					$('#success_auth').show();
					$('#infoAuth').show();  
				}else if(errorData.responseText == "already"){
					//alert('Already registered member mail');
					alert(eval('msgP.'+langCode+'.aleadyUser'));
				}else{
					$('#error_auth').show();
				}

			}
		});
	
	});

	$("#join2reSend").on("click",function(event){
		$("#join2SmsSubmit").click();
	});


	//회원가입 Submit 액션
	$("#join2submit").on("click",function (event) {    
		//Email 체크
		var regex = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;
		var email = $('#username').val();
		if (regex.test(email) === false) {
            $('#error_email').show();
            $('#username').val('');
            $('#username').focus();
			return false;
        }
		
		var pswd = $('#password').val();
		    
		var pattern = /^.*(?=.{8,16})(?=.*[0-9])(?=.*[a-zA-Z]).*$/;

		if(!pattern.test(pswd)) {
	    	alert(eval('msgP.'+langCode+'.passwdChk'));
	    	$('#password').focus();
	    	return false; 
	    }
		    
		//비밀번호 체크
		var p = $('#password').val();
		var pConfirm  = $('#passwordConfirm').val();
		if(p != pConfirm){
			$('#error_passwordConfirm').show();
            $('#passwordConfirm').val('');
            $('#passwordConfirm').focus();
			return false;
		}

		//국가코드 체크
		if($('input[name=country_code]').val()==''){
			//alert('국가코드를 선택해주십시요.' + $('input[name=country_code]').val());
			alert(eval('msgP.'+langCode+'.nationCd') + $('input[name=country_code]').val());
            $('input[name=country_code]').val('');
            $('input[name=country_code]').focus();
			return false;
		}
		
		//휴대폰번호 체크
		if($('#phone_number').val().length == 0){
			alert(eval('msgP.'+langCode+'.insMobile'));
            $('#phone_number').val('');
            $('#phone_number').focus();
			return false;
		}
		
		//인증번호 체크
		if($('#authCode').val().length == 0){
			//alert('인증번호를 입력해 주십시요.');
			alert(eval('msgP.'+langCode+'.insAuthcode'));
            $('#authCode').val('');
            $('#authCode').focus();
			return false;
		}
		/*
		//구글 체크?
		if(!recaptchaChecked){
			alert(eval('msgP.'+langCode+'.chkRecaptcha'));
			return false;
		}
		*/
		$(".loading-group").show();
		$('#myform').attr('action','/User/regiAction').submit();

    });


});


function recaptchaCallback() {
	//alert('recaptchaCallback !!!!!!!!!!!!!!');
	recaptchaChecked = true;
};
