




function validateEmail(email) {
	var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	return re.test(email);
}

function initFooter(){
	//csrf 설정

	var imgSrc = $('html').find('img').eq(0).attr('src');
	var langCode = imgSrc.substring(imgSrc.indexOf('.net')+5, imgSrc.indexOf('/images'));
	var langArray = "en|ko|ru|jp|cn";
	if(langCode == null || langArray.indexOf(langCode) < 0){
		langCode = 'ko';
	}
	
	$("#btn_join").css("cursor","pointer");
	$("#btn_join").on("click",function (event) {
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");	
		
		var email = $("#join_mail");

		if(email.val()==""){
			//alert("정보를 입력하십시오.");
			alert(eval('msgP.'+langCode+'.insEmail'));
			email.focus();
			return;
		}else if(!validateEmail(email.val())){
			//alert("메일 정보가 유효하지 않습니다.");
			alert(eval('msgP.'+langCode+'.correctMail'));
			//alert()
			
			email.focus();
			return;
		}
		
		var search = {}
	    search["email"] = email.val();

		//alert(JSON.stringify(search));
		
		$.ajax({
			type: "POST",
			contentType: "application/json",
			url: "/Join/join",
			data: JSON.stringify(search),
			dataType: 'json',
			cache: false,
			timeout: 600000,
			beforeSend: function( xhr ) {
			    xhr.setRequestHeader(header, token);
			},
			success: function (data) {
				//alert("등록되었습니다.");
				alert(eval('msgP.'+langCode+'.registered'));
			},
			error: function (e) {
				var errorData = JSON.parse(JSON.stringify(e));
				if(errorData.responseText == "success"){
					alert(eval('msgP.'+langCode+'.registeredMail'));
					//alert("이메일 등록이 되었습니다.");
				}else{ 
					//alert("이메일 등록에 실패하였습니다. 잠시후 다시 시도해주시기 바랍니다..");
					alert(eval('msgP.'+langCode+'.failRegMail'));
				}
			}
		});
    });
}

$(function(){
	  'use strict';

	  initFooter();
	});

